# code contributions not accepted
hi, i do not accept contributions.
this is because it is my website.

any issues related to the website itself can be raised on the issue tracker, or just email me if you can't/won't create an issue on gitlab.

# website guidelines
## don't break permalinks
permalink changes (e.g. changing URL format for posts) should kept to an absolute minimum so that any past links that were made would still work.

if determined to not be absolutely necessary, it will not be done.

if a post is modified, use the `last_modified_at` attribute in the front matter instead of changing the date.

## intellectual property
don't violate them, including infringing copyright, violating trademarks or patents.

