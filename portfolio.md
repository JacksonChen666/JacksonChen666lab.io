---
title: Portfolio
description: Portfolio of things I made in the programming world
---
## Portfolio
**Note:** Most of these projects were done with the help of internet resources (i.e. [stackoverflow](https://stackoverflow.com)) and existing solutions.
- **Python (beginner and intermediate-ish *not* self taught)**
    - *Twitter API*
        - [Last interactions](https://gitlab.com/JacksonChen666/TwitterLastInteractions)
        - [Bill Wurtz might quit song lyrics on a twitter thread](https://gitlab.com/JacksonChen666/BillWurtzMightQuitTwitter)
    - *Discord bots*
        - [Discord bot full of features that discord has as commands](https://gitlab.com/JacksonChen666/Discords-Essentials)
        - [Assign roles based on the level up messages on MEE6 bot.](https://gitlab.com/JacksonChen666/AutoRole)
        - *Discord bots (private repo)*
    - *Other*
        - [Random ideas made into python](https://gitlab.com/JacksonChen666/Random-Python-Ideas)
        - [Random ideas made into python series 2](https://gitlab.com/JacksonChen666/Random-Python-Ideas-2)
        - [A "module" that can manipulate video information.](https://gitlab.com/JacksonChen666/video_manipulator)
- **Java (self-taught)**
    - *Spigot plugins*
        - [Minecraft treecapitator plugin](https://gitlab.com/JacksonChen666/treecapitator)
        - [Recreating items from Hypixel Skyblock](https://gitlab.com/JacksonChen666/HypixelSkyblockRecreations)
        - [Plugin for writing server PID into a file](https://gitlab.com/JacksonChen666/PIDFile)
        - [Putting what's on the screen into Minecraft](https://gitlab.com/JacksonChen666/screen-in-minecraft)
        - [Making everything explode in Minecraft](https://gitlab.com/JacksonChen666/ExplodingEntities)
    - *Bungeecord plugins*
        - [Add the amount of servers online on the bungeecord onto the MOTD](https://gitlab.com/JacksonChen666/BungeecordMOTDServerCount)
- **Kotlin (self-taught)**
    - *Spigot plugins*
        - [Minecraft with a limited number of lives](https://gitlab.com/JacksonChen666/Lives)
- **Web stuff**
    - *HTML, JS, and the related stuff (self-taught)*
        - [My website](/)
        - [Another set of pages made with SSGs](https://gitlab.com/JacksonChen666/microblogging.jacksonchen666.gitlab.io) which can be found live [here](https://microblogging.jacksonchen666.com/)
    - *ReactJS (self-taught)*
        - [multi-flexible-timer](https://gitlab.com/JacksonChen666/multi-flexible-timer)
- **Mobile app stuff**
    - *Flutter/Dart (self-taught)*
        - [Stopwatch for rubiks cubes basically](https://gitlab.com/JacksonChen666/rubiks-cube-stopwatch) 
