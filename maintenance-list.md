---
title: maintenance
permalink: /maintenance/
---
maintenance/incidents posts have been superseded by the status subdomain.

[privacy policy for status website]({% link privacy/website/status.md %})

please follow either link to the status page:
- [clearnet](https://status.jacksonchen666.com/#incidents)
- [tor onion service](https://status.x7ikq7gwf6vnbvrc7b36nkcxnw7eckwaricmjbdvrajoeql2ccjb5aad.onion/#incidents)

[clearnet and tor onion links reference]({% link mirrors.md %})
