# jacksonchen666.com

my website

# running a local version

clone the repo first

## dependency prerequisites

[`asdf`](https://asdf-vm.com) can be used to install dependencies like `ruby`
and `bundler`.

however, if you do not prefer installing `asdf`, here's the versions used by
this project (from .tool-versions):
- bundler 2.3.11
- ruby 3.0.3

both should be installed. you can then run `bundle install` and it should work.

## bundle configuration

optional (but sometimes the opposite is required, like the CI): configure
bundler to avoid the 'ci' group, with the following: `bundle config set --local
without "js_runtime"`

## install dependencies

run `bundle install` in the folder

## serve the site

run `bundle exec jekyll s` (optionally include `--livereload` to reload on
changes) and visit `127.0.0.1:4000` (or `localhost:4000`) to see the website

# expected production site generation

- the environment variable `JEKYLL_ENV` must be set to `production`
- don't include the `--lsi` flag when building.
  [reasoning](https://jacksonchen666.com/posts/2022-01-06/20-50-29/)
- the `CI_COMMIT_SHA` environment is set to a full git commit SHA and is
  accurate
- the correct respective configuration file is used (`_config.yml` for main,
  `_config-tor.tml` self-explanatory) with the `--config` variable in jekyll

# environment variables that can affect the output

- `JEKYLL_ENV` (optional)
    - when set to `development`, files will not be uglified, and source maps
      are available. not intended for production.
    - when set to `production`, files will be uglified and source maps are not
      available. not intended for development.
- `CI_COMMIT_SHA` (semi-optional)
    - can be any arbitrary value
    - used in the "source" button in the navbar as a `title` html attribute
    - should be the full git commit SHA

# licensing

this project is compliant with the [REUSE
specification](https://reuse.software/spec/). please refer to the `LICENSE`
file for more information.

# contributing

contributions are not accepted. please refer to CONTRIBUTING.md for more info.
