---
title: search this website
---
# this is no longer available.

this is because trying to figure out how to get it working in hugo was never
happening.

it would be better if you just downloaded my entire website and use `grep`
to search instead of relying on client sided javascript with hundreds of
dependencies with a single library (yes, a single library with **hundreds of
dependencies**).

instructions to download and find:
1. go to
   [here](https://gitlab.com/JacksonChen666/JacksonChen666.gitlab.io/-/jobs)
2. find a job that is named "pages"
3. click the download button
4. unzip the downloaded file
5. find text using your preferred tool in the `public-no-gzip` directory of
   the extracted zip file

alternatively:
1. `git clone
   https://gitlab.com/JacksonChen666/JacksonChen666.gitlab.io.git`
2. grep the cloned repo

alternatively:
use your preferred search engine
