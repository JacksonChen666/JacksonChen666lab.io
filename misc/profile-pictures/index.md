---
title: profile pictures
description: list of profile pictures + history
---
- [github pfp](https://github.com/identicons/JacksonChen666.png)
- [gitlab
  pfp](https://secure.gravatar.com/avatar/d0423f73a5b7cd51975bb75dafaf7264?s=800&d=identicon)
- a [clock](https://commons.wikimedia.org/wiki/File:Another-Clock.gif) (used
  on [mastodon](https://micro.jacksonchen666.com/@jacksonchen666))
- mc skin pfps (with christmas hat, [direct
  download](https://files.jacksonchen666.com/mc_pfps/) or through
  [IPFS](https://ipfs.io/)
  `/ipfs/QmUPQRBG7wbwo6kJ4ZpkCEWemyr2ESUjeWQXPJQCRQL99h`).
  - version 1 (in mid-air, since 2021-06-12)
      - not transparent
          - strokes (479 KB PNG)
          - no strokes (367 KB PNG)
      - transparent
          - strokes (408 KB PNG)
          - no strokes (209 KB PNG)
  - version 2 (closer up, 2022-03-12?)
      - normal (most used)
          - actual thing (3 MB PNG)
          - lossy thing (196 KB PNG)
      - broken sun (reddit)
          - actual thing (187 KB JPG)
