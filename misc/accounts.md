---
title: my accounts
description: where you'll find my accounts
redirect_from:
- /misc/socials/
---
if you want to contact me, [go to the right place](/contact)

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

MY ACCOUNTS

note: accounts claiming to be me (impersonating me) are not mine if not
listed here, or the account is marked DELETED or NONEXISTENT.

these aren't reliable ways to contact me. don't expect a response

CENTRALIZED SERVICES:

instagram: DELETED

facebook: DELETED

twitter: DELETED on 2022-12-01

reddit: DELETED on 2022-09-16

youtube: @jacksonchen666
(https://www.youtube.com/channel/UCut1u2mQy5LMZw5UKgOQKjA)

roblox: @JacksonChen29A (https://www.roblox.com/users/214789171/profile,
@JacksonChen666 on roblox is not me. they managed to somehow take my idea
before i thought of it.)

tiktok: DELETED on 2022-01-29T16:04+01:00 (data archived personally)

odysee: NONEXISTENT

oftc.net irc: jacksonchen666

libera.chat irc: jacksonchen666

hackint.org irc: jacksonchen666

DECENTRALIZED STUFF (FEDIVERSE):

peertube (videos): @jackson@videos.jacksonchen666.com
(https://videos.jacksonchen666.com/c/jackson/videos)

mastodon: @jacksonchen666@micro.jacksonchen666.com
(https://micro.jacksonchen666.com/users/jacksonchen666)
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQQFdTkmENxv1Ly2EW3yQfmHGx0cZAUCY7lvLAAKCRDyQfmHGx0c
ZLsKAQD1iLlUkTtk80/5yMEaVEMnSTofOdTdk5Q4pxyFSXwYJAEAy/5aSQ4OF9Fo
B8JBkaINqjxRQ53OtqPWafolNT/qMgM=
=6ddy
-----END PGP SIGNATURE-----
```
