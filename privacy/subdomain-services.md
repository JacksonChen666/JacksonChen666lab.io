---
title: subdomain services privacy policy
description: privacy policy for subdomain services
redirect_from:
  - /general-other-privacy/
---
last updated: 2022-11-20

# applicability
this general privacy policy applies to all subdomains under jacksonchen666.com excluding the `microblogging` and `status` subdomains, and under all domains and subdomains of the onion service[^onion].

# notes
this is a general privacy policy that applies to multiple different services.

because of the nature of the services being different, this general privacy policy will apply alongside the dedicated privacy policy for that service.

# summary
- some information sent from the client, and sent back to the client, are logged.
- logs are deleted every monday, with backups up storing info up to 2 weeks
- logs made by the serving application are kept in it's default configuration.

# data stuff 
this section is about data handling, retention and other related things.

## collected information

### server logs
these are the following information that is logged by the reverse proxy, nginx:
- [virtual host](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/host) the client requested
- the IP address of the client
- provided username for HTTP basic authentication
- time of request (up to 1 second of precision)
- request method 
- request path
- responded status code
- amount of bytes the client sent in the body
- [Referer header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer) given by the client
- [User Agent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent) given by the client

### application logs
the services being hosted may log information on it's own.
the information about what is logged will not be documented here, but on the service specific privacy policy.

### user submitted information
information that the user submits is stored for the service's functionality when necessary (e.g. username).

## personal information
we do not expect to deal with personal information.

personal information does not include IP addresses.

if personal information does however end up on our services and we discover it, we will remove it as soon as found.

## data retention
the logs are rotated[^rtt] every week (starting on monday) and all older copies are discarded.

an additional 2 weeks should be added if accounting for backups.

## data sharing
data is not shared with third parties unless absolutely necessary (e.g. complying with legal orders).

## opt out
a global opt out is not offered, yet. however, an individual opt out may be available for a specific service for only some of the things.

[^onion]: the onion service is included in this privacy policy because it uses the same server for serving the onion site, which has the same policies nonetheless.
[^rtt]: rotated meaning when the rotation period has been reached, the current log file will be deleted (not wiped) and a new log file will be created in place of the old file.
