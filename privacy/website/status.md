---
title: privacy policy for status.jacksonchen666.com
---
last updated: 2023-02-05

# applicability
this privacy policy applies to webpages and websites on <https://status.jacksonchen666.com> and <http://status.x7ikq7gwf6vnbvrc7b36nkcxnw7eckwaricmjbdvrajoeql2ccjb5aad.onion>.

when using the latter domain to visit the website, the [onion services privacy policy]({% link privacy/subdomain-services.md %}) also applies.

# summary
- we do not collect personal information

# third party services
this website is hosted by [sourcehut], provided by their service [sourcehut pages](https://srht.site/). see the [privacy policy for sourcehut][sourcehut] for further information.

# data stuff
this section is about data handling, retention and other related things.

## collected information
we use a self-hosted instance of plausible to collect analytical data about the usage of this website.
however, we no longer collect anymore analytical data.

the analytics data for the main website is not yet publicly available.

for detailed information about what the self-hosted instance of plausible is collecting/processing, please see the [plausible data policy](https://plausible.io/data-policy).

## personal information
we do not handle any personal information.

## data retention
the analytical information is stored indefinitely.

## data sharing
we do not share personal information with third parties.

## opt out
analytics has been removed, and there's no analytics to opt out from on the website.

[sourcehut]: https://man.sr.ht/privacy.md
[plausible]: https://plausible.io/
[status]: https://status.jacksonchen666.com/
[gitlab]: https://about.gitlab.com/privacy/
