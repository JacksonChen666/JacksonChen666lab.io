---
title: privacy policy for microblogging.jacksonchen666.com
---
last updated: 2023-02-06

# applicability
this privacy policy applies to webpages and websites on <https://microblogging.jacksonchen666.com> and only on <https://microblogging.jacksonchen666.com>.

# summary
- we do not collect any kind of information

# third party services
this website is hosted by [sourcehut], provided by their service [sourcehut pages](https://srht.site/). see the [privacy policy for sourcehut][sourcehut] for further information.

# data stuff
this section is about data handling, retention and other related things.

## collected information
we do not collect any personal information.

[sourcehut]: https://man.sr.ht/privacy.md
