---
title: privacy policy for jacksonchen666.com
---
last updated: 2023-02-05

# applicability
this privacy policy applies to webpages and websites on <https://jacksonchen666.com> and only on <https://jacksonchen666.com>.

# summary
- we don't collect personal info intentionally
- we don't share any personal info with third parties
- submissions with personal info will be removed
- submissions cannot be removed completely once published because of technical limitations[^1]

# third party services
this website is hosted by [sourcehut], provided by their service [sourcehut pages](https://srht.site/). see the [privacy policy for sourcehut][sourcehut] for further information.

# data stuff
this section is about data handling, retention and other related things.

## collected information
we previously provided a way for the user to submit questions and ideas (however, we no longer do so).
it is called "submissions", where the user may input any text into the text box and submit it as a question or an idea.
the information submitted is stored on the receiving server until processed.
when processed, the information is published, at which point it is not possible to completely revert the publication.

see the [personal information](#personal-information) for the handling of personal information for if we do collect any unintentionally.

---
we use a self-hosted instance of plausible to collect analytical data about the usage of this website.
however, we no longer collect anymore analytical data.

the analytics data for the main website is not yet publicly available.

for detailed information about what the self-hosted instance of plausible is collecting/processing, please see the [plausible data policy](https://plausible.io/data-policy).

## personal information
we do not handle any personal information about any users or visitors.
however, it was possible for users to submit text ("submissions"), which may potentially lead to the user submitting personal information.
when personal information is found, it is deleted immediately.

## data retention
any submissions that was published is kept indefinitely, because it is impossible to permanently due to technical limitations[^1].

---

analytics data is stored indefinitely.

## data sharing
we do not share any personal information with third parties.

## opt out
analytics has been removed, and there's no analytics to opt out from on this website.

[^1]: the technical limitations is that the submissions are stored in git. git can be distributed, and published changes are difficult to revert for all copies of the submissions.

[sourcehut]: https://man.sr.ht/privacy.md
[plausible]: https://plausible.io/
[opt out]: /plausible-exclusion
