---
title: template privacy policy
description: my template for a privacy policy for my stuff
---
last updated: 2022-11-02

COMMENT: while made for my website, it is written in third person. who knows the intention to do so.

NOTE: you can add custom sections if you think you need them.

TODO: update last updated date

TODO: don't lie

# applicability
TODO: describe when this privacy policy is applicable, like on which domains or applications. keep it short but accurate and eliminating edge-cases.

# summary
TODO: make a *really* short summary. like bullet points few words short.

# third party services
TODO: document third party services that you are using that are worth noting (like gitlab pages hosting the website), and their privacy policy.

# data stuff
this section is about data handling, retention and other related things.

## collected information
TODO: in subheadings (high detail) or bullet points (low detail), document all information you collect from your users.

## personal information
TODO: describe what you do with and how you handle personal information. even in the case the user inputs it when you don't expect them to do so, or you just get them naturally (*ahem* IP addresses).

## data retention
TODO: say how long the data is kept for, while also accounting for backups and other copies.

## data sharing
TODO: document anyone you share your data with.

## opt out
TODO: if you offer opt out, document all of it here. if not, give good reasons.

