---
title: privacy policies
descriptions: privacy policies for my stuffs
---
due to the sheer complexity and fragmentation of the privacy policy applying differently, the pages has been split for each different subject.

- [jacksonchen666.com website privacy policy](/privacy/website/main)
- [status.jacksonchen666.com privacy policy](/privacy/website/status)
- [microblogging.jacksonchen666.com privacy policy](/privacy/website/microblogging)
- [email privacy policy](/privacy/email)
- [subdomain services privacy policy](/privacy/subdomain-services)
- [onion services privacy policy](/privacy/subdomain-services) (this applies to all onions, including the main website)

[template privacy policy](/privacy/template) (for consistency)
