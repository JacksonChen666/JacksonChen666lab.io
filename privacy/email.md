---
title: jacksonchen666.com email privacy policy
description: privacy policy for sending me emails
---
last updated: 2022-11-02

# applicability
this privacy policy is applicable when sending emails to any users on jacksonchen666.com, or using the public email available on the [contact page].

# summary
- all emails i receive is collected
- all copies of the emails are stored privately

# third party services
[gandi], the domain registrar and email provider handles the hosting of the email on jacksonchen666.com. [gandi privacy policy][gandi pp]

[simplelogin] is an email forwarding and aliasing service. applicable for public email address available on [contact page]. [simplelogin privacy policy][simple pp]

# data stuff
this section is about data handling, retention and other related things.

## collected information
all information you sent in an email, including information stored in the headers and body, is stored.

## personal information
any personal information that is sent will be kept.
the email can be deleted under certain circumstances, most likely on request.

## data retention
the emails are removed from the mail servers when possible, but a privacy copy of the emails is kept indefinitely.

## opt out
what would opt out even mean for sending emails? don't send me any emails?

[gandi]: https://www.gandi.net/
[gandi pp]: https://www.gandi.net/en/contracts/privacy-policy
[contact page]: /contact/
[simplelogin]: https://simplelogin.io/
[simple pp]: https://simplelogin.io/privacy/
