---
title: learning rust, a documented journey of the beginnings of rust
category: [programming, rust]
author: jacksonchen666
date: 2022-07-01T17:08:07+02:00
tags: [learning, beginner]
---
so, i wanted to learn a new programming language, rust.
except, i have not read [the entire book](https://doc.rust-lang.org/book/title-page.html) and have not made any progress on it or the knowledge of the language itself *for months*.
so, today, i decided to take a try at rust with adding `1+1`, and then over complicating the procedure by adding more complexity and process to learn about things in a reasonably fast fashion (with the help of the internet resources).

# target audience
the target audience is people who already know how to do programming (and have what i would consider, about an intermediate understanding of programming concepts) and use the command line (the basic level is probably enough).
that's mostly based on me and my experience with programming and stuff, and yeah that's it. maybe.

# code license note
(rust) code listed here is licensed under the [WTFPL license](https://gitlab.com/jacksonchen666/hello-rust/-/blob/main/LICENSE).

# initial motivation
> i want to learn rust!

\*crickets\*

> i want to create a functional real-world project in rust!

\*crickets\*

> i'm going to rewrite [this entire project](https://gitlab.com/jacksonchen666/quick_statement_generator_for_software_version_identifier) in rust!

\*crickets\*

> fuck it, it's way too much. let me do `1+1` in rust and overcomplicate it so i learn some shit instead of bikeshedding.

and then this happened.

# installation
for some odd reasons i did not have rust installed from rustup.
so to install, [go here](https://www.rust-lang.org/learn/get-started), run a command to run some scripts and more, and install the rust toolchain with default options.

and then it is done, not much else.

# new project
so you gotta make a project.
this time, ~~we~~ i will be making a project that takes something so simple (i.e. `1+1`) and then overcomplicate the procedure of adding numbers enough to learn about things.
i hope that from my experience, you can learn something too if you're also new to rust.

so to start off, a project needs to be created (somewhat repeated statement).
to create a project, run outside your intended project directory the following command: `cargo new <project_name>`.
`<project_name>` will become the project name, and the command will create a folder named the project name you gave.
alternatively, you can use `.` as the `<project_name>` to create the project in the current directory basically.

## initial project: hello world
when you create a new project with cargo, you will get a hello world program which you could use as a test on your compiler and stuff.
the project should compile. if it doesn't, figure out your own problem because i didn't.

## adding numbers
adding numbers is pretty simple. it's `1+1`.
no code to show here, it is literally `1+1` to add 1 and 1 together.

## printing numbers
now let's print the number.
let's try...

```rust
println!(1 + 1);
```

ok, my editor (neovim) says that doesn't work, because `println!` needs a string.

### wait, what's that exclamation mark doing?
it means that it's a [macro](https://doc.rust-lang.org/book/ch19-06-macros.html).
that's about it.

---

how about the following:

```rust
println!((1 + 1).to_string());
```

still doesn't work.
let me check the compiler with `cargo check` in my editor

```
:!cargo check                                                                                                                                                                                                                                                          
    Checking hello-rust v0.1.0 (/Users/jackson/Desktop/Everything/Pending/hello-rust)
error: format argument must be a string literal
 --> src/main.rs:7:14
  |
7 |     println!((1 + 1).to_string());
  |              ^^^^^^^^^^^^^^^^^^^
  |
help: you might be missing a string literal to format with
  |
7 |     println!("{}", (1 + 1).to_string());
  |              +++++

error: could not compile `hello-rust` due to previous error
```

wow that's... actually helpful...
let me just fix that and it now works, cool!
and that's done.

well, there's some extra stuff like the `to_string` function, which i think is redundant.
removing the call did not result in an error.

## create a function that adds 2 numbers
```rust
fn print_math() {
    println!("{}", add_num(1, 1));
}

fn add_num(num1: i64, num2: i64) -> i64 {
    num1 + num2
}
```
(truncate code because)

yes, there's no return statement.
because actually, it is implicit for the last statement.

you could be explicit about what's returned, but where's the fun with ambiguity and confusion without an explicit return statement?

well actually that wouldn't be fun.
use `return` when needed instead.

that aside, you'll need to declare the parameters with the types it accepts, and the type of the value you will return.

mostly smooth sailing here.

## creating another function that adds a list
ok so, what's the equivalent of a list of items in rust?
well, i found array, but [that's fixed size](https://stackoverflow.com/questions/34684261/how-to-set-a-rust-array-length-dynamically#34684869) and assume that we are dealing with arrays that have different sizes.
so, what's a dynamically sized basically array?
[vectors](https://doc.rust-lang.org/stable/rust-by-example/std/vec.html).

```rust
fn add_num(num1: i64, num2: i64) -> i64 {
    let nums: Vec<i64> = vec![num1, num2];
    add_multiple_num(nums)
}

fn add_multiple_num(nums: Vec<i64>) -> i64 {
    let mut num = 0;
    for i in nums.into_iter() {
        num += i;
    }
    return num;
}
```

so, the `add_multiple_num` function just sums it up.
get it? sums it up... ah nevermind.
it loops over the "array" (vector) and adds to the variable holding the numbers.
then finally, returns the number

## let's do some borrowing
up until this point, the code was *basically* backwards compatible with past code.
this time, a breaking change will be made, and it is a mostly important one for performance reasons.

what performance reasons?
well, i saw a video (which i could no longer find) comparing the performance of rust and c++.
rust was normal and c++ took forever long to do some kind of processing of some sort.
after reading the comments, i've determined that the comparison is just a joke, gives rust an unfair advantage by slowing down c++ by giving a function large amounts of data instead of passing it like where the data is, and is a cherry picked example.
maybe that could be the reason why it's important, but meh, it's not official and i haven't found much information or evidence that rust would have the same.

anyways, vectors can be any size.
it can be really large.

AND THE CHANGE IS adding an ampersand before the parameter type and the variables that are passing to the function. Like the following:

```rust
fn add_num(num1: i64, num2: i64) -> i64 {
    let nums: Vec<i64> = vec![num1, num2];
    add_multiple_num(&nums)
}

fn add_multiple_num(nums: &Vec<i64>) -> i64 {
    let mut num = 0;
    for i in nums.into_iter() {
        num += i;
    }
    return num;
}
```

that was about... 2 characters in total on 2 different lines.
not much to [see](https://doc.rust-lang.org/beta/rust-by-example/scope/borrow.html) here.
i guess that's done.

## a function that adds to the passed variable
more with borrowing, this time with mutable borrowing.

so here's what i came up with:

```rust
fn add_mut_var_num(mutvar: &mut i64, num: &i64) {
    mutvar += num
}
```

what does the compiler have to say this time?

```
> cargo check
    Finished dev [unoptimized + debuginfo] target(s) in 0.19s
```

oh... nothing...
let's try running the program instead

```
:!cargo run
   Compiling hello-rust v0.1.0 (/Users/jackson/Desktop/Everything/Pending/hello-rust)
error[E0368]: binary assignment operation `+=` cannot be applied to type `&mut i64`
  --> src/main.rs:24:5
   |
24 |     mutvar += num
   |     ------^^^^^^^
   |     |
   |     cannot use `+=` on type `&mut i64`
   |
help: `+=` can be used on `i64`, you can dereference `mutvar`
   |
24 |     *mutvar += num
   |     +

For more information about this error, try `rustc --explain E0368`.
error: could not compile `hello-rust` due to previous error
```

now it's yelling at me, it also tries to help me, and it's adding a deference thing.
add it, and it works.

now, using the function, it's a little bit different.
taking the addition in the `add_multiple_num` for loop and replacing it with the function.
what could go wrong?
well, things did go wrong, because it was wrong.

here's the code of the function i tried to replace the adding function for:

```rust
fn add_multiple_num(nums: &Vec<i64>) -> i64 {
    let mut num = 0;
    for i in nums.into_iter() {
        num += i;
    }
    return num;
}
```

see that `num += i;` thing?
replaced it with `add_mut_var_num(&num, $i);`.

```
:!cargo run
   Compiling hello-rust v0.1.0 (/Users/jackson/Desktop/Everything/Pending/hello-rust)
error[E0308]: mismatched types
  --> src/main.rs:18:25
   |
18 |         add_mut_var_num(&num, &i)
   |                         ^^^^ types differ in mutability
   |
   = note: expected mutable reference `&mut i64`
                      found reference `&{integer}`

For more information about this error, try `rustc --explain E0308`.
error: could not compile `hello-rust` due to previous error
```

now it doesn't help... but i remember something useful, which is a mutable borrow passed to the function of course, replacing `&num` with `&mut num` which makes things work.

---

well, i guess that's all for today's work.

there's not much else i have on the list, and the post is getting pretty long too.
so i guess see you next time (or not).

oh and just in case you were wondering about the code, it is [here](https://gitlab.com/jacksonchen666/hello-rust).
