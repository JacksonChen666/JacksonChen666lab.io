---
title: opening my "ssh server" and seeing what happens
author: jacksonchen666
date: 2022-01-04 08:20:41 +0100
last_modified_at: 2022-03-15 00:22:57 +0100
categories: [ssh, experiments]
migrated: true
---
[endlessh, an ssh tarpit that sends banners. forever.](https://github.com/skeeto/endlessh)
last commited on 2021-04-30, the piece of software still works to this day and seems to be working in 2022.

i opened up port 22 (don't get too excited yet) to just waste people who might be trying to break in with default credentials (or something else). the thing is, i don't even have an ssh port open to the public to begin with (technically).
pretty sure i opened it up at 2022-01-02, cause the logs for endlessh in systemd says a lot of restarts then endlessh logs come in. it's 2022-01-04 as of writing.

2500+ connections were opened and closed, spending a very precise time of 50 seconds, 90 seconds, basically inhumanly precise times (up to 0.01 seconds of margin of error). the byte size is different between all for some reason, but that could be correlative with the time spent waiting.

unfortunately, i didn't get anything more than 150 seconds or longer connection, even if the [author](https://nullprogram.com/blog/2019/03/22/) got connections that lasted up to many many hours in **2019**.

# what i've learned
not changing the public port for ssh is likely a bad idea. and a common alternative choice is probably a bad idea too. but also, i'm just a rando with a server.
