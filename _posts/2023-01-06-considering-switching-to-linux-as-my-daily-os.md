---
layout: post
title: considering switching to linux as my daily OS
date: 2023-01-06T02:14:48+01:00
---
i am writing this blog post in neovim on my m1 macbook running linux with
the help of [asahi linux]. i have setup my PGP keys and also painfully setup
kmail (given how unstable the experience was, maybe i should stay on using
thunderbird for emails since i'm used to it anyways).

[asahi linux]:https://asahilinux.org

now the question is: should i ditch macOS?

a better question to ask: should i move everything from macOS to linux?

# feature support needs

here's the problems i still have with asahi's feature support:
- (not documented?) night light[^nightlight]
- external displays (HDMI, 4k60hz)

that's about the last 2 things i basically need before i would consider
switching to linux as my daily driver.

here's problems i've been experiencing while using asahi linux:
- battery life while usage is not great, especially when i'm only in the
  terminal writing... text (like i am doing right now)

  i should state that sleeping (on edge kernel) does *greatly* reduce
  battery usage, with the caveat that you must press the power button to
  wake the mac up. i suspect the GPU being always active could be the
  culprit, but i could be completely wrong and just expecting insane hours.

# complications

- my email workflow is that if i'm done with it, i archive it. archiving the
  email is local, and the emails are then no longer accessible on the mail
  server. 
- if i want to move my thunderbird setup, i would have to copy all the data
  over to linux. if i do that, there will be an inconsistent state, so it's
  best to have one active copy of my thunderbird data around.
- my data is stored on drives formatted with (encrypted) APFS. linux support
  for (encrypted) APFS is... probably non-existent.
- i don't have all the storage space in the world, so moving data from
  (encrypted) APFS to ext4 (or btrfs?) could be a lot of work alone.
- different shortcuts, breaking workflow
- i need to figure out the tools i have on macOS, then translate that over
  to linux. which is also work, work which might be a lot also.

# answer to a question

should i move everything from macOS to linux?

no, definitely not right now. maybe later.

[^nightlight]:<https://oftc.irclog.whitequark.org/asahi/2023-01-06#1672965966-1672966694;>
