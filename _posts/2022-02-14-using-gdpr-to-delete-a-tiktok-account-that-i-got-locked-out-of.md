---
layout: post
title: using GDPR to delete a tiktok account that i got locked out of
date: 2022-02-14 23:59:30 +0100
author: jacksonchen666
categories: [gdpr]
---

spoiling the results: it was deleted.

# history

so, musical.ly was a thing, and i did signup (probably around 2016). but ever since tiktok bought and merged itself with musical.ly, i never got around to getting any login method working for that musical.ly account (migrated to tiktok) to actually be able to sign in.
i still had access, but through another phone. however, musical.ly was deprecated and later, gone. the app no longer works (due to it contacting non-existing servers), and so i was locked out of my account because i only had one login method and that doesn't exist on tiktok anymore.
after that happened, yeah there wasn't much.

# until i learned about GDPR right to be forgotten much much later

it is basically legal requirement for data keepers to respond to erasure requests for users within europe basically.
i was a europe user, and much later when i learned about it, i sent an erasure request through the email that i know belongs to the old musical.ly account.
i sent an email to some email addresses, got turned down and was forced to use a form that had a limit (which i might've increased the max length client sided which may or may not have worked, idk) and cut away the message i originally sent.
after some waiting, tiktok nicely responded with "ok, we banned your account and we'll be deleting your tiktok account in 30 days".

now thinking about it, i'm not sure if it was because i abused the form or it's just the process, but the account is definitely gone.

# notes
no, this isn't about the tiktok account with the username of @jacksonchen666. that was created in response of me being locked out of my previous account. it also had the username @jacksonchen0 before i came up with what is now now, so yeah.

# conclusion
the tiktok account was deleted.
