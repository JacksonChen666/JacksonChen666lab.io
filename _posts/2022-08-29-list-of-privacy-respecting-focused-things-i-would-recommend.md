---
layout: post
title: list of privacy respecting/alternative front-end things i would recommend
author: jacksonchen666
categories:
- privacy
date: 2022-08-29T14:07:00Z
last_modified_at: 2022-09-05T06:38:04+02:00
---
in the modern world, there's things like youtube, reddit, twitter, instagram, etc.
you may have concerns over your privacy even though the only thing you do is read text, watch videos, or look at pictures and you don't even have an account. and well, yeah, it would still be reasonable to assume that without an account, the services can still track you.
so, you may want to use an alternative front end. but 

note: web software (like nitter) do not link to instances, but source code. you will need to find instances on your own.

- instagram: [bibliogram](https://sr.ht/~cadence/bibliogram/) - i have not tried this (mostly because instagram is what i don't even use at all)
- twitter: [nitter](https://github.com/zedeus/nitter)
- youtube (desktop): [invidious](https://invidious.io/)
- youtube (android): [newpipe](https://newpipe.net/)
    - youtube (android, [sponsorblock](https://sponsor.ajay.app/) & [return youtube dislike](https://returnyoutubedislike.com/)): [newpipe with sponsorblock fork](https://github.com/polymorphicshade/NewPipe) ([name may change in the future](https://github.com/polymorphicshade/NewPipe/discussions/164))
- ~~youtube (ios, requires sideloading): [uYou+](https://github.com/qnblackcat/uYouPlus) (basically vanced but for ios. anti feature: upstream is closed source code)~~ this isn't much of privacy respecting or an alternative front-end, but rather something like vanced for iOS basically. use at your own risk.
- reddit: [teddit](https://codeberg.org/teddit/teddit)
- wikipedia: [wikiless](https://codeberg.org/orenom/Wikiless) ([reasons to use wikiless](https://codeberg.org/orenom/Wikiless#user-content-why-i-should-use-wikiless-instead-of-wikipedia))
- automatic redirects (most browsers): [redirector extension](https://einaregilsson.com/redirector/)

i have used all except bibliogram and wikiless. otherwise, looks pretty good to me.

well, this was mostly short (or long if i extend this). i hope this was helpful, and goodbye.
