---
migrated: true
title: the inefficiency of string concatenation (at a level of typing)
author: jacksonchen666
tags:
- javascript
date: 2021-12-12 12:53 +0100
---
so you don't know, i'm working on a project of timers.
dealing with date and times and parsing the things, i've come to a realization of the following:
to type `variable + " " + variable` (add space between 2 strings) would require my thumb to alternate from shift to space 9 times for the ` + " " + ` part. **the inefficiency!**
yes, i could just ignore the spaces between the `+` sign, so it's like `variable+" "+variable` and so only 3 alternations are required.
does string concatenation have to be like that? no. here's what i did instead:
```js
var variable = `${variable} ${variable}`
```
much more readable code without the mess of `+` characters.
