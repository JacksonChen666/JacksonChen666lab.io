---
layout: post
title: accessibility is not just about blind people
categories: [accessibility]
date: 2023-01-14T17:27:09+01:00
# vim:textwidth=76
---

if you think the statement in the title is insane, please be open minded for
the rest of this post (and more if you wish to continue to be open-minded).

---

let me rewrite the word "accessibility": ability to access.

can your readers:
- access your website on a [slow internet connection][dialup]?
- read your content with *vision impairments*?
- use [old hardware][oldhw] and still be reasonably fine with it?

[dialup]:https://en.wikipedia.org/wiki/Dial-up_Internet_access
[oldhw]:https://drewdevault.com/2019/01/23/Why-I-use-old-hardware.html

now moving out of the norm zone:
- can your readers read with [links]?
- with [lynx]?
- with email: can they read the [email in plain text instead of
  html][plainemail]?

[links]:https://en.wikipedia.org/wiki/ELinks
[lynx]:https://en.wikipedia.org/wiki/Lynx_(web_browser)
[plainemail]:https://drewdevault.com/2016/04/11/Please-use-text-plain-for-emails.html

in real life:
- can people in wheelchairs access your place?

(i couldn't think of anymore, but there is definitely more type people to
find)

and in any place:
- do they understand the language you're communicating in?
- have you considered people lacking one more of their [senses]?

[senses]:https://en.wikipedia.org/wiki/Sense

finally, i'll be ending this post with my words on this list: the caveats
for this list is that it's *very incomplete*, and does not even account
*most* of the things you could be (or should be) accounting for. you should
probably read the [web content accessibility guidelines][WCAG].

[WCAG]:https://www.w3.org/TR/WCAG21/
