---
migrated: true
title: i have 0 reasons to upgrade to macos monterey because it's just bugs and nothing new and useful (rant)
date: 2021-12-22 21:59 +0100
categories: [rant]
tags:
- macos
author: jacksonchen666
last_modified_at: 2022-08-24T22:03:28+02:00
---
and i upgraded anyways

imo, macos monterey lacks good reasons to upgrade to (personally), and have reasons to not upgrade to, with minor bugs and annoyances that i've experienced.

# my experience
i started writing this after the preferences window for amphetamine didn't focus after first opening. that means if windows tries to get your attention, they probably can't in the first place.

this also applies to blender. maybe my browser. and even system preferences. heck, even disk utility.

i have no idea how it's caused, why it's there.
the chances of it happening? also kinda random and unpredictable.

writing this, i realize that i have a backup of my system before i upgraded to macos monterey. but because i have the m1 mac with 1 true recoveryos (1tr) that upgrades with the system, i probably won't be restoring my system.

also, the microphone indicator. it's software, in my recordings, and can't be turned off. just leave me alone, i want to record a clean output and not slightly littered with orange dots.

airplay? i unfortunately found no use. not all features are for everyone, including me.

# decisions for the future
switch to linux. how? [asahi linux](https://asahilinux.org/). when? ~~when *it* is done~~ when asahi has enough support enough for me to work like i do with macos (though with different keyboard shortcuts and some workflows). why? it's probably nicer there (linux), or i just feel like it. yeah i guess i'd feel like doing it. maybe i'll just have a macbook with linux inside and not macos. or that i just plan on using linux after i no longer have an unnecessary obligation to use a mac. it's not just caused by macos monterey, it's also me wanting to use linux on mac or just use linux really.
