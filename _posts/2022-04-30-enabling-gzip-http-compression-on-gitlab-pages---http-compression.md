---
layout: post
title: enabling gzip http compression on gitlab pages & http compression
author: jacksonchen666
categories:
- website
- http
tags:
- http compression
- gitlab pages
date: 2022-04-30 15:15 +0000
---
oh hey cool, now my website does less data transfer with the same amount content on [jacksonchen666.com][website] (note for my onion sites[^1]). but the question is "how"?

# how to enable http compression on gitlab pages
compress each file with [gzip] or [brotli] according to [documentation][gitl http comp docs]
that's it. publish your site with it (or your [CI][own compress method] with it) and now you're serving with option to use compressed assets, which is likely to reduce size needed to transfer for the same content.

## command to gzip
doing gzip on all the files might not be the easiest.
but using `gzip` in a debian system (not in an alpine system), specifying the directory and the `-r` (recursive) flag and also a compression level of `9` (with `-9`)

or you can just use the [example on gitlab docs][gitl http comp docs]

## my own method - compress and remove unworthy compression
i use a method on my onion version of my website, which i [copied to my CI][own compress method] which was modified beforehand to guarantee to work in the specific image (debian based).

it removes files that are compressed, but that only result in less than 10% compression ratio (taken from output of gzip).
not sure if that's actually working, but whatever i guess.

## client support
which web browser client would support it?

most major browsers i guess, but actually only when [the right header and value][Accept-Encoding] is sent.

# word of warning on http compression
[http compression] reduces the size required to transmit data.

unfortunately, it does have [security implications](https://en.wikipedia.org/wiki/HTTP_compression#Security_implications), mainly that according to [this website](https://breachattack.com/) (which has an expired certificate as of writing this sentence) is that compression with reflected user data and secrets might make your website valuable to the [BREACH] attack.

however, that doesn't really apply to complete static sites like github pages or gitlab pages, so there's not much other than having your site compressed or not (i can't guarantee that github pages will support http compression, do your own research on that)

# further reading
- [http compression]
- [gzip]
- [brotli]
- [Accept-Encoding]
- my [own compress method]
- [BREACH (wikipedia)][BREACH]
- [BREACH (dedicated website)](https://breachattack.com/)
- [CRIME](https://en.wikipedia.org/wiki/CRIME)
- ["Whats the best custom compression method to use when I have SSL?"](https://security.stackexchange.com/questions/39913/whats-the-best-custom-compression-method-to-use-when-i-have-ssl/39914#39914)
- ["BREACH - a new attack against HTTP. What can be done?"](https://security.stackexchange.com/questions/39925/breach-a-new-attack-against-http-what-can-be-done)
- ["Is HTTP compression safe?"](https://security.stackexchange.com/questions/20406/is-http-compression-safe)

wow am i doing actual research for some text on a blog post and i'm too intimidated of the sheer theoretical size of research so i don't do for videos?

[^1]: sites that i serve over onion has already been gzip compressed (mostly) since [BREACH] attack is the least of my concerns when it is end-to-end encrypted with http and not just https
[Accept-Encoding]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding
[gitl http comp docs]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#serving-compressed-assets
[own compress method]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/blob/bff794c466f4475932736c76e86e43ca58a724f6/.gitlab-ci.yml
[http compression]: https://en.wikipedia.org/wiki/HTTP_compression
[BREACH]: https://en.wikipedia.org/wiki/BREACH
[gzip]: https://en.wikipedia.org/wiki/Gzip
[brotli]: https://en.wikipedia.org/wiki/Brotli
[website]: https://jacksonchen666.com
