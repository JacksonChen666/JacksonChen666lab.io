---
title: moving my blog to a new place
author: jacksonchen666
categories:
- website
- blog
tags:
- new blog
- old blog
date: 2021-12-01 12:36 +0000
last_modified_at: 2022-04-14T14:50:09+02:00
pinned: false
---
ok so, i've experimented.
i like it better that i can just write and publish without all of that overhead that i have with the main website. and the overhead with the main website isn't worth to make a blog with.
so, new blogs (including drafts that i have since) will all be over at <https://blog.jacksonchen666.com>.

# an extra nice
my instance of writefreely is [activitypub](https://en.wikipedia.org/wiki/ActivityPub)-federated, it's [@jackson@blog.jacksonchen666.com](https://micro.jacksonchen666.com/web/accounts/107360277138982806).
you could use replies as comments on the blog too.

# the old blog
it will stay as to not break links, again.
not all new posts will be posted here, or at all.

# tor users (if they even exist)
append `blog` to the subdomain of the onion address *should* work.
otherwise go to <https://blog.jacksonchen666.com> first and then upgrade to onion (if you don't get the option or it doesn't redirect, the thing is probably not working).

i'll also figure out onion address upgrades after this post.
