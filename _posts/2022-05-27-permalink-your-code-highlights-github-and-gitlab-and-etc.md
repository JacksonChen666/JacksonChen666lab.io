---
layout: post
title: permalink your code highlights (github and gitlab and etc.)
author: jacksonchen666
date: 2022-05-27T09:29:22+02:00
categories: []
tags: [github, gitlab, potential non-sense words]
---
so i was looking at some comments [here][discussion] and found a link that linked to code.
link that highlights a part of the code.
it is not a permalink, so what i see is [this][my view].
do you know how unhelpful that could be? given enough time, very unhelpful.

so, what's the solution?
using permalinks with code being partially highlighted, like you wanted but you also "freeze" the view in time.
this method is known to work on github and gitlab, but anything else is up to you to figure out when it doesn't work.

1. copy permalink of specific commit of the code (branch linked is not a permalink, commit linked would be a permalink)
2. select the part of the code you want to highlight
3. copy the link in the URL bar of your browser

and after pasting the link in a comment and assuming you're done it correctly, you paste the link and format it however you want (probably only when you know how to format text).

if you would like an example, [this can be one](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/blob/abcdef4e5d67340af1e5ff8bb907f370dbed3319/_posts/2022-05-27-permalink-your-code-highlights-github-and-gitlab-and-etc.md#L18-20) (you'll have to press the "display source" button to see the highlighted section which looks like `</>`).

[discussion]: https://github.com/tidwall/sjson/issues/43#issuecomment-791688412
[my view]: https://github.com/tidwall/sjson/blob/133db2881f4d53a2513463f3dcdcbd6a1ae6639d/sjson.go#L59-L63
