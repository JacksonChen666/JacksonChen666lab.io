---
layout: post
title: 'not onioned: my term explained'
permalink: "/not-onioned/"
author: jacksonchen666
categories:
- onion
tags:
- server
date: 2022-04-15 13:43 +0000
last_modified_at: 2022-12-03T20:15:56+01:00
---
i'm assuming you came from my subdomain page where you found out that there's actually nothing (like a service or page) there, and were confused by the term "not onioned".

if you didn't come from there, would the above matter? no.

well, i'm here to say that it is related to me changing the webserver to replicate the feel of my website over clearnet even more.

the reason for a service not onioned can be anything. but here's the current situation:
- micro: ~~mastodon returns 403 forbidden trying to access it over tor. couldn't figure out a solution for that.~~ i just needed to add a config option named `ALTERNATE_DOMAINS` documented in the [basic](https://docs.joinmastodon.org/admin/config/#basic) section, not the [hidden services](https://docs.joinmastodon.org/admin/config/#hidden-services) where it's mostly undocumented. it now works over tor.

  also it says to expect URLs to be requested over the clearnet, so there's not too much benefit if you want strictly over onion.
- videos: i kinda just didn't bother, but also, peertube nginx configuration is highly complex so unfortunately, no.
- chat & matrix: the client will probably use the clearnet anyways as suggested by the homeserver, and onionizing the cleanet will also probably cause false sense of security (if there is security in using onions instead). some related synapse issues: [#5152](https://github.com/matrix-org/synapse/issues/5152) [#7088](https://github.com/matrix-org/synapse/issues/7088)
- grafana: not supported

if you try to go on such places over the hidden service, you'll get redirected to the clearnet.
that is intentional, for full functionality of my services and stuff.

