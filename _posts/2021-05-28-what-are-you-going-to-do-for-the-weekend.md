---
title: What are you going to do for the weekend?
author: jacksonchen666
date: 2021-05-28 23:33:56 +0200
last_modified_at: 2022-08-25T16:56:41+02:00
---
In conclusion: literally nothing doable (please give ideas thanks)

Really, I have run out of things to do for the weekend, and then I'll have a 2 month break after I finish stuff, where I can do anything, but I have not much planned that I can really do (most of the ideas I have is for the future (which might not be a good idea)).

If you have any idea suggestions for programming, website, or even Arch Linux related, please do go to the [Ideas and Suggestions](/ideas) page to submit ideas. If it breaks, please report on [GitLab](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues), cause it has happened before.

Now what should I do?
Make more things that I can do and make a deadline or actually do all the things I planned for later sooner?
Maybe I should do my more important stuff then work on other stuff, so I don't screw myself up.
