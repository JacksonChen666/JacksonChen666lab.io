---
layout: post
title: cross-compiling gentoo - a slightly helpful guide to install non-native architecture/embedded
  gentoo systems
author: jacksonchen666
categories:
- gentoo
tags:
- riscv
- aarch64
- linux
- embedded
- cross-compiling
date: 2022-05-31 17:32 +0000
last_modified_at: 2022-10-01T11:21:30+02:00
---
hey! have you ever wanted to compile riscv (target) gentoo on aarch64 (host) gentoo? or do cross-compiling[^1] of gentoo because there's no minimal installation CD from the gentoo teams or just for the sake of it?
yeah i wanted to. that's because at the time of trying to do this (or even writing this, 2022-05-31), there's no minimal installation CD for riscv architecture (stage3 tarballs are available though).

while there's already a handbook called the [embedded handbook] (more specifically [this thing](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot)), i found that i had to scour for more resources and information so i can find the right commands and the right arguments so that things would work.

# aside: my experience with installing gentoo on aarch64 using the amd64 handbook
i used the [amd64 handbook] to install a gentoo system on aarch64 (in a VM), because there was [no arm64 handbook](https://wiki.gentoo.org/wiki/Handbook:Main_Page#Viewing_the_Handbook) going to be available (not even like a general one?).

it works actually. and because there's also no handbook for riscv (yet), we are going with the [amd64 handbook] for installation.

# a word of warning
*this installation method is probably not supported by gentoo*.

please *avoid* filing bugs or ask for support through official channels, unless you're absolutely certain that people *will* agree with you that it is a supported method of installation (i'm pretty sure it is *not*).

if you wish to ask for support or file for a bug anyways, please also link to this post if you are using it as a method of installation.

the author might not provide support for you, the reader.

# requirements
## target audience
this blog post targets the audience of people who:
- want to install gentoo with cross-compiling[^1] instead of the minimal install CD whether it is available or not
- already know how to install gentoo
- are able to generalize commands to work in your own case (if needed, probably not idk)
- knows how to substitute and knows what to substitute as appropriate
- understand this... thing (including linux, command line)

if you don't meet the expected targeted audience, you might struggle with this slightly helpful guide to cross-compiling[^1] gentoo.

## hardware and software
you'll need:
- a computer/VM with gentoo installed and working (preferably on the native architecture for performance)
- more time than your usual gentoo installation (might want to not use the desktop profile because it can take *longer*)
- some software
    1. install/emerge `app-emulation/qemu` with [specific USE flags](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot#Package_configuration) set.
       
       also create the binary package for the `qemu` package as it is needed later. use `quickpkg` and specify the package name.
    2. get any necessary tools like `gdisk` for disk partitioning (if you're gonna take the raw file and then `dd` it basically)
- a kernel with `CONFIG_BINFMT_MISC` set to `m` (module) or `y` (yes). if you don't have it set, enable the option and do your kernel compiling dance.

# notes and assumptions
- you could probably transform this guide into a guide for creating another gentoo install, without messing with different architectures.
- this guide assumes that you'll mount the "disk" to `/mnt/gentoo`
- this installation guide will assume you want to setup a [loop device] that's using a file `truncate`d to a specific size you want, like `16G`.
    - `truncate`ing a file to a specific size will create a hole in the file that takes up less space (or in a practical sense, truncate a file to the specified size).
- **all commands here are being run as root**

# the main part
follow the [amd64 handbook] like it's made for the (still missing) riscv handbook (gentoo handbooks are mostly the same AFAIK).

some sections have to be skipped/replaced with other steps instead documented here. read carefully and don't forget.

1. start with ["About the installation"](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/About), and continue until ["Choosing the media"].
2. when in the stage of ["Choosing the media"], reconsider if you really need the steps, since you're probably doing it on your main system.

   and just get the tarball for the architecture you want.
3. continue from ["Configuring the network"](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Networking) until you reach ["Preparing the disks"], at which you take a detour with the following:

    1. create a file with `touch <filename>` substituting `<filename>` with the file name/path you choose (also replace subsequent `<filename>` with the one you chose here)
    2. create a hole in the file, by doing `truncate -s <size> <filename>` substituting `<size>` with the desired size.
       
       for something like 16 gibibytes (GiB), use `16G` for `<size>`.
    3. setup a [loop device] with `losetup --show -PLf <filename>`, and note down the printed path for future use like disk partitioning.
   
   after that has been done, you may continue ["Preparing the disks"] and using the new loop device that was just setup instead of a physical media.

   note: you may need to use `partprobe` so that the kernel knows that there are changes to the partition table, and re-reads the partition table.
4. from the ["Preparing the disks"] section, continue until you reach ["Installing base system"](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Base) at ["Entering the new environment"], taking another quick detour:

    1. start from ["Register binary format handlers"](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot#Register_binary_format_handlers) (only running the necessary command for the target architecture to save some time), and stop when you reach ["Setup chroot"](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot#Setup_chroot), deviating from the guide:

        1. run `ROOT=$PWD/ emerge --usepkgonly --oneshot --nodeps app-emulation/qemu` with `$PWD/` substituted with the root directory of the new environment (not necessary if your current working directory is the path of the new environment you're trying to install)
        2. run `chroot /mnt/gentoo /bin/bash` to chroot

           if you get an error along the lines of "file not found" for `/bin/bash`, you probably forgot the `static-user` USE flag for the `qemu` package. re-emerge on your host with the new USE flag, create the binary package again, and retry from step 4.1.1.
        3. run `echo 'EMERGE_DEFAULT_OPTS="$EMERGE_DEFAULT_OPTS --exclude app-emulation/qemu"' >> /etc/portage/make.conf` and `emerge --noreplace app-emulation/qemu` inside the chroot to prevent `qemu` in changes which breaks the emulation layer

           note: when done with `chroot`ing, changes in `make.conf` just above should be reverted (or deleted), and `emerge --depclean`ing `qemu` is highly suggested because of the architecture incompatibility (if there is) of the program.

           note 2: if the `emerge` command that excludes qemu from being yeeted doesn't work, `emerge-webrsync` may be required. then retry the command.
5. continue with ["Entering the new environment"], except skipping already done commands which is `chroot /mnt/gentoo /bin/bash`, until you reach ["Configuring the bootloader"] at ["Rebooting the system"], and take another detour:
    
    note: if you think you need `chroot` from your host later, **do not follow these steps**.
    1. delete `EMERGE_DEFAULT_OPTS="$EMERGE_DEFAULT_OPTS --exclude app-emulation/qemu"` from `/etc/portage/make.conf` in the environment
    2. (warning: don't do while `chroot`ed) run `emerge --depclean app-emulation/qemu` to purge the wrong/non-native architecture `qemu` in the environment
6. continue with ["Configuring the bootloader"] at ["Rebooting the system"], and maybe avoid rebooting your host system lol.
7. after unmounting, it is wise to detach the loops that was setup. do that with `losetup -d /dev/loop<number>`

and that's about it for all of the things you pretty much need to do.

the result should be a gentoo system inside of a disk image as a file.
now do whatever you want i guess.

# resources
- <https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot>
- <https://wiki.gentoo.org/wiki/Handbook:AMD64>
- <https://wiki.gentoo.org/wiki/Binary_package_guide#Creating_binary_packages>
- <https://github.com/qemu/qemu/blob/master/scripts/qemu-binfmt-conf.sh> in case you need `x86_64` cross-compiling and the first resource link refuses to provide that

# footnotes
[^1]: it's actually kind of like "architecture emulated compiling"

[embedded handbook]: https://wiki.gentoo.org/wiki/Embedded_Handbook
[amd64 handbook]: https://wiki.gentoo.org/wiki/Handbook:AMD64
["Choosing the media"]: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Media
[loop device]: https://en.wikipedia.org/wiki/Loop_device
["Preparing the disks"]: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Disks
["Entering the new environment"]: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Base#Entering_the_new_environment
["Configuring the bootloader"]: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Bootloader
["Rebooting the system"]: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Bootloader#Rebooting_the_system
