---
layout: post
title: new blog is deprecated
author: jacksonchen666
tags:
- new blog
date: 2022-06-04T16:38:45+02:00
last_modified_at: 2022-07-03T15:32:36+02:00
---
after more consideration, i've decided to non-irreversibly deprecate [new blog].

this decision was based on the factors of actual usage, and results from having the thing.

# reason 1 - a new self-contained era of phones
when i got my new phone, it was android.
the OS i installed didn't have a password manager built in, so i got another.
i was unable to decrypt the passwords on my phone (because something wasn't supported) resulting in me unable to access my passwords on my phone.
so i wrote blog posts in a text editor instead, which is basically what i also do on [new blog], just in a web browser instead.

because of the less use of the website to write the blog posts and the ability to do similar while offline and locally (well, in the writing phase),
i've decided that writing blog posts on the phone with the web interface is no longer a good use case.

# reason 2 - improper support for footnotes
i use some footnotes sometimes, but [it doesn't work][^needlink] on writefreely.
this post itself will be on [new blog](https://blog.jacksonchen666.com/new-blog-is-deprecated), and you can see the problem compared to [old blog]({% post_url 2022-06-04-new-blog-is-deprecated %}).

# counterargument 1 - activitypub federation
"but with activitypub, you allow people to comment on your blog posts that way!"

my argument is:
- there's no interface for comments/replies on [writefreely](https://github.com/writefreely/writefreely)
- i don't even check the comments on those posts
- basically no one uses them (well, at least for my blog)
- i can require much less[^less] to comment

---

that's about all of the reasons why i decided to deprecate [new blog], which pretty much [most of the reasons i started new blog]({% post_url 2021-12-01-moving-my-blog-to-a-new-place %}).

# the future of new blog
i will not make further posts on it, or synchronize the posts on there.

new blog will stay up indefinitely until things break, which will probably result me in turning off [new blog] indefinitely.

i have saved the posts and drafts just in case for fast restoring (if that works), or future reference (drafts that still haven't been published).

# the future of old blog
i will now call it [main blog](/).

[new blog]: https://blog.jacksonchen666.com/
[^less]: and by much less, i mean not require a domain name and a server running on the domain name with activitypub federation ability that done correctly. oh and an account on that server that is able to access my domain over the clearnet, which when compared to a closed source centralized commenting system that only requires the name and the comment, that's a lot.
[^needlink]: help! i can't find the link to the issue i'm talking about. maybe search on github? thanks!
