---
title: finally fixing delegation on my matrix server
author: jacksonchen666
categories:
- matrix
date: 2021-11-01 11:35 +0000
---
yep, i have done the impossible.
fixing the damn delegation for the matrix server, and i managed it because of this [same issue that was resolved](https://github.com/vector-im/element-web/issues/14969#issuecomment-674379552).
but at the same time, i feel conflicted about the comment in the configuration (for `public_baseurl`), but only for the first two sentences:

> The public-facing base URL that clients use to access this Homeserver (not including _matrix/...). This is the same URL a user might enter into the 'Custom Homeserver URL' field on their client.

there was documentation that said to not use something like matrix.domain.tld, because you "wouldn't do the same for email.domain.tld".
but also, the third sentence was the following:

> If you use Synapse with a reverse proxy, this should be the URL to reach Synapse via the proxy.

so, it should've been matrix.jacksonchen666.com because that's how i set it up.
yeah uhhhhhhh

that said, currently my element client isn't working as expected (because it is being redirected to the www subdomain, what a shame) but at least the [web version works](https://chat.jacksonchen666.com/)

# moving on forward with the www subdomain
[in my matrix server post]({% post_url 2021-10-08-i-have-a-matrix-server-now-i-guess %}), i mentioned that i moved my blog to the www subdomain.
well, no more, because delegation seems to be working as expected.
i'll proceed to yeet users still going to the www subdomain to the @ subdomain.

# existing users that still can't access the matrix server (0 users still affected, excluding @jacksonchen666)
log out and log back in.
fixes everything except the loss of encrypted message.
hope you have a way to get it back!

# "help, it's not working"
if your client doesn't support delegation for some reason, then idfk use matrix.jacksonchen666.com as custom server.
or you could use <https://chat.jacksonchen666.com>

## fully anticipated question: why are you typing in lowercase?
because why spend the effort of upper-casing letters when you can type completely lowercase (i also do it on reddit if you didn't notice)
