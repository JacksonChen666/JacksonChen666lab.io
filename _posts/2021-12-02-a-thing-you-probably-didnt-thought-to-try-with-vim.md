---
migrated: true
title: a thing you probably didn't thought to try with vim
date: 2021-12-02 09:40 +0200
author: jacksonchen666
categories: [vim]
tags:
- discovery
---
right, you use vim or not, you might've not known about what i've accidentally discovered too long ago to care about when i discovered the thing.

so, let's start with the basics you should already know (if you use vim): `h`, `j`, `k`, `l` are navigation keys. type a number before the navigation key and you'll move that many lines/characters in a direction. `p` pastes, and typing numbers before `p` repeats paste that many times.

well, let's say you're in an unlikely/likely scenario: you need to somehow write a line of something, copy that line, and then repeat it by pasting it nth times.
while you can use `yy` to copy/yank a line and then `p` to paste the line, such operation can be reduced down to `ni` with `n` being the number of times to repeat what you inserted.
so that means, you can tell vim how many times to repeat what you've just inserted/appended compared to copy pasting copy pasting copy pasting (should've used vim for this but who cares).
