---
title: GitHub, GitLab, and the future of everything I make
author: jacksonchen666
date: 2021-07-10 17:13 +0200
last_modified_at: 2021-10-10 21:12:39 +0200
tags: [github, gitlab]
---
I'm moving to GitLab with only the active projects I still do.

Because GitHub is owned by Microsoft and closed source, which means, it's owned by Microsoft and closed source. What else could it be, an open source platform that's closed source? lol it is. What else not be the same but at least open source? GitLab.

Github, in the very least, is.... loving of open source in such a way that they don't open source GitHub themselves. Yes, there are reasonable assumptions that maybe they don't want to release the source code because security and abuse. But if you're lucky enough, the amount of good people finding bugs may out compete the bad people.

I've gave up on GitHub and I'm moving on from GitHub. GitHub co-pilot's training data is just code, without consideration of any copyrights and licensing, so it's Microsoft being Micro$oft I guess, and I don't like such practice.

Yes, I acknowledge that I still use Microsoft VSCode. But at least I'm not the one that [uses 5 different services that are the parent or owned by Micro$oft](https://twitter.com/fireship_dev/status/1385236991457955845).

## The future of everything I make, and the past
All the old stuff will be left on GitHub, archived. If I am going to work on a new thing related to programming, [GitLab](https://gitlab.com/jacksonchen666) is where I will be at. Anything that's old and not worked on will still be on GitHub.

## More on GitLab whatever
I got an email address finally, for git commits. You can email me. Don't email to my personal email addresses, as I will delete your email and ignore it. You could click the email button that's at the bottom of my entire site to open up whatever might be the handler of the mailto protocol, instead of being rickrolled.
