---
author: jacksonchen666
title: ok so... new years
category:
- something
layout: post
date: 2022-12-31 22:33 +0000
---
so for the last post of the year (at least in UTC and my... +01:00 timezone
(DST is just additional unnecessary complexity)), i am going to write about
random things including about my website.

# website's hugo migration

as i go further into (already) finishing the [hugo migration](
https://gitlab.com/JacksonChen666/JacksonChen666.gitlab.io/-/milestones/5),
i finished the remaining items by getting rid of the source of the problem
(search, submissions, post summaries). that should reduce the duplicated
contents a bit. just a by a bit.

# new year... or something

if you feel like there's not much to celebrate on new years except for a new
year (new number), [you're not really
alone](https://mastodon.social/@Azoor/109610313529316134).

# subdomain services

going to <https://fallback.jacksonchen666.com/> (the fallback page listing
all the services), you can see that i have separated the monitoring services
from actual services. this creates for a realization that many of the
services i host is actually either for the service itself, or just
monitoring the service.

## prometheus

with the [alerting sourcehut has with
prometheus](https://metrics.sr.ht/alerts), i also setup [my own
alerts](https://prometheus.jacksonchen666.com/alerts). currently, it isn't
covering many things (like disk space).

# sourcehut

it's possible i'll migrate <https://status.jacksonchen666.com> to using
[sourcehut](https://sourcehut.org/) as a way to get my feet wet with
sourcehut CI.

they offer [website hosting](https://srht.site/)[^caveats] and
they seem to be better at open source than gitlab (AGPL + other vs MIT +
proprietary stuff).

# new invisible writing style

invisible\*

in this post, i have changed my writing style so that i can `/---^MggnjgqG`
in vim (`^M` is enter key) and then format my post so that it is wrapped in
the characters specified in the `textwidth` setting of my neovim. this will
be an interesting one, as it aims for plain text readability and not plain
text readability.

# goodbye ipfs, i guess

i have thrown the ipns link in my mirrors page out of the mirrors page.
i don't think i'll be maintaining the ipns link anymore, so i'll be removing
references to it.

# goodbye monero (and cryptocurrency)

after being influenced by [drew's blog post about
cryptocurrency](https://drewdevault.com/2021/04/26/Cryptocurrency-is-a-disaster.html)
and a followers-only toot[^drew-toot] (on [this
account](https://fosstodon.org/@drewdevault)) suggesting destruction of
private keys instead of cashing the crypto out, i have decided to destroy
the private keys.

this included a lot of mined monero, totaling... 0.01? 13 USD.

i also have a bit of bitcoin but fees are more than my bitcoin so they were
basically worthless.

# goodbye twitter

i deleted my twitter account on the perfect[^timing] day so that it will be
gone by 2023-01-01.

i've already moved away from twitter announcing on twitter it will be
deleted sometime soon... and that time will be coming[^timing].


# out of topics

at this point, i should stop writing this blog post. i am out of topics to
write about, and i am being distracted by the people talking near my area so
that will definitely limit my focus abilities.

so, again, enjoy the footnote.

## why do i have so many things to add
why do i have so many things to add i just added ipfs monero twitter writing
style i am running out of time for 2022-12-31 aaaaaaaaaaaaaaaa

---

[^timing]: at least, 1 day before it would be deleted on 2023-01-01
[^caveats]: with some [caveats](https://srht.site/limitations)
[^drew-toot]: > If you hold cryptocurrency and want to get out, don't sell it -- that'll just create a new victim. Do you have the guts to burn your private key instead?
