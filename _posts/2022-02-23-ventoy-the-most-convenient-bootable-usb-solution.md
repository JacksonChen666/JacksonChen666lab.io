---
layout: post
title: ventoy - the most convenient bootable USB solution
date: 2022-02-23 13:14:14 +0100
author: jacksonchen666
---
disclosure: i'm simply endorsing software, and i'm not affiliated with the developers of ventoy.

hey, do you use a USB flash drive (will be referred as USB/USB drive) to boot linux images for whatever purpose? do you ever have to rewrite the data on the USB to use another image? maybe [ventoy][v] can help a lot because you can store as many ISOs as your USB drive can and boot any choice of the ISOs already stored of the USB drive and lots of ISOs already work out of the box?

let me share my experience: it works very well for most things, and it's helpful in some ways.
having ventoy installed on a USB stick actually doesn't mean much to your free space (it's like less than 100 MB) or normal use of your USB stick (normal being files but no raw partitions[^1]) but does mean a lot to the convenience.

if i ever wanted to install linux on my moms computer (for some reason), i'd have to boot ubuntu to check what to do to disable settings to make the disks show up (in that case, it's [Intel RST](https://en.m.wikipedia.org/wiki/Intel_Rapid_Storage_Technology)), and then i would boot into my preferred distro of choice (if i'm installing for myself). 
that could be how it goes, but i haven't done it tho.
if i didn't have [ventoy][v], that would have probably been more time consuming for me.

so yeah, that's about it unfortunately. you can go and get [ventoy][v] now.

[^1]: at install time, you can set aside some space for other misc. partitions that aren't just filesystems

[v]: https://ventoy.net/
