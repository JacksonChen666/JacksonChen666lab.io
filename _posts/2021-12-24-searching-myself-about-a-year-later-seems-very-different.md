---
migrated: true
title: searching myself about a year later seems very different
date: 2021-12-24 16:22 +0100
categories: [random]
tags:
- searching myself
author: jacksonchen666
---
a year ago (or so), i searched my name up. today, it's me, with my website being first (we win these), but also containing a lot of other sites that just straight up copied tiktok videos and then just put it on their site for some reason.
this time, it's not there.

yeah that's just one thing. another thing is that i would have search results with me involved in threads on socials, stack exchange, etc. i think. today, it's still the same: if i'm involved, i'm probably in there.

well, now to the more weird part

# a weird result
duckduckgo, and search for images. there seems to be an image to a thing... a website... that's barely good english... contains a lot of weird `?` characters... it just seems to scrape everything on the web and then compile it into text, and it's getting stupidly high results in the images section... it's not even related to me, but mentions me only once, and it seems familiar: username/repo-name. it was the hypixel stuff recreation that i never worked twice on. and then some github text... and then it continues with forum stuff...

i tried to archive with web.archive.org. it only redirected them to instagram (after multiple redirects of course). tor browser? just fine and still weird (didn't check where it was routed to). so i started personally archiving it.

# unrelated results
- [power outage in southwick, ma?](https://poweroutage.report/southwick-massachusetts) ok, they have related tweets. i tweeted about power outages before. but i also never lived in that place.
- a peertube video. ok, i have an instance. and a channel. it probably appeared and that would also explain it.
