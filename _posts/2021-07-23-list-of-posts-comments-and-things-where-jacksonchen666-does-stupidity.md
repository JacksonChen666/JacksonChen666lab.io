---
layout: post
title: List of Posts, Comments, and Things where jacksonchen666 does Stupidity
author: jacksonchen666
permalink: /stupids/
date: 2021-07-23 03:25 +0200
last_modified_at: 2021-07-24 18:26:25 +0200
---
Welcome to a post on my website.
You probably got linked here, and I'll explain why this even exists.

The reason this page exists to list the times I felt that I was correct (or in another way of correct), but actually was wrong the whole time.
Those are the types of mistakes I'll list, but not the non-factual mistakes I make.

That means I won't include some silly (debatable?) mistake where I incorrectly write [the wrong year at the top of the post](https://www.reddit.com/r/billwurtz/comments/l40flm/the_music_video_here_comes_the_sun_was_rendered/) that does still happen to me in different ways)

[I guess it explains why I'm downvoted on more of programming stuff or whatever.](/assets/images/posts/stupids/per-subreddit-votes.png)

OK, now on to the format. I will be using [hyperlinks](https://en.wikipedia.org/wiki/Hyperlink), and also rank it by the level of stupidity, so referencing the things on this page by number is probably a bad idea, because in the very future I might do the dumbest thing possible.

1. [Time complexity doesn't work because there isn't really an input size, so the time complexity is equal to a constant.](https://www.reddit.com/r/badcode/comments/opc5ef/the_antizen_of_python/h65csvq/)
2. [.io is NOT the intended abbreviation for input/output (just for nerds)](https://old.reddit.com/r/confidentlyincorrect/comments/p82cyn/dot_com_is_short_for_company/h9na6ww/)
2. [Signed int underflow become positive numbers, not negative numbers](https://www.reddit.com/r/softwaregore/comments/oct8a9/i_like_my_odds/h3wsexv/)
3. ["did you read the title?" (well, i only read some random keywords)](https://www.reddit.com/r/programming/comments/p2a0mv/curious_to_know_how_was_this_website_created_i_am/h8is9ez/)
