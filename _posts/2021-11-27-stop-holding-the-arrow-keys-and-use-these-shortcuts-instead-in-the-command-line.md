---
title: stop holding the arrow keys and use these shortcuts instead in the command
  line
author: jacksonchen666
categories:
- command line
tags:
- time saver
date: 2021-11-27 22:33 +0100
---
alright, you're using the terminal.
you have a long command, and you need to go to the start of the command to change the command that you're gonna run.
it's gonna take a while (relative timing), and you know that the home key can put your cursor at the starting place of the command but you don't have a home key.

..........

yeah it's taking a while.

but in the book i'm currently 1/4 way through called ["The Linux Command Line" (Fifth Internet Edition)](https://linuxcommand.org/tlcl.php), and here's what i've learnt on page 106:

use `Ctrl-a` to move to the beginning of the line, and use `Ctrl-e` to move to the end of the line.

recently (like maybe a few minutes ago), i've used it, and it has saved more time than holding `Alt-left`, which moves by "words"[^1] to the left and not by the entire line.
i would imagine that when people who haven't heard about that shortcut hear about it now, it's kind of a game changer.

[^1]: well, not words defined by a separation with the space character, but also stops on underscores. nice QoL thing.
