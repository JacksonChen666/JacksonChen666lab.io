---
layout: post
title: marginal website improvements post because why not
author: jacksonchen666
categories:
- website
tags:
- jekyll
date: 2022-03-27 14:22 +0000
---
i was bored, so i just started looking around for things that could be improved. and i improved them marginally.

i might not do these improvement posts about my website again, but whatever.

# new things
## <abbr title="Table of Contents">ToC</abbr>
there's a table of contents for every post on this site (unless there isn't any headers, which would result in an empty ToC), which is based on the headers in the document.

because? why not.

## reading time
i also added another thing, which is reading time.
the [plugin](https://github.com/bdesham/reading_time) works on the assumption of 270 wpm, so i added that note too.

because? why not.

## more link colors
now there are link colors for visited link, and external links (and the visited version of it).

because? there wasn't a default visited color link thing.

# moving things around
## categories and tags
they have been moved to a hidden by default detail section thing called "Additional metadata"

because? to take up less space.

# saving myself work
## external links going to external tabs
i've done that manually, but that's still a lot of work.

so i added a plugin to do that for me, and removed all existing things.

also used the plugin to add external link colors.

## lazy image loading
same as external links, add plugin and remove existing work. works.

## saving myself work reasoning
because.
