---
migrated: true
title: the deep underlying workings of a http request (by guesswork and some stuff that's true)
date: 2021-12-18 13:02 +0100
categories: [guess work, http]
author: jacksonchen666
---
or a slightly extensive description of the workings of http and a bit of other stuff

you've made a http request to this blog. now how does that work?

high level: send some data to server

a bit lower level:
1. your machine queries dns servers that's setup (usually in system settings or settings provided by the network). a dns server is usually an ip address your machine sends a request to (e.g. 1.1.1.1) or info is gotten from the cache (likely reducing the need to recursively resolve dns queries)
    1. the recursive dns resolver queries the dns root servers, which is multiple servers having information on domain tlds (e.g. .com) but not domain names or even subdomain names (e.g. jacksonchen666.com, blog.jacksonchen666.com)
    2. the recursive dns resolver after querying for .com dns servers from dns root servers, queries for jacksonchen666.com
    3. after querying for jacksonchen666.com which gets resolved to my domain registrar dns servers, it asks them for blog.jacksonchen666.com
    4. the recursive dns resolver finishes resolving for blog.jacksonchen666.com and returns the ip address (dns A and AAAA records) to the requesting client
2. your machine now has the ip address to send the http request to, and so does.
  - the http request from the client to the server usually contain the following:
    - headers
      - a notable http header is `Host`: it tells the server what domain it is requesting for even though the request is actually sent to an ip address.
    - data (if any, usually none for http GET request)
3. my server responds with the contents of the web page with a http request.

now, another example with <http://videos.jacksonchen666.com>, with slightly (pfftt *slightly*) more detail. let's say you visit that after visiting my blog and maybe even this post.
1. your machine queries the recursive dns resolver again
  1. the recursive dns resolver has some cache, and so does not need to resolve .com, jacksonchen666.com, except for videos.jacksonchen666.com, and so it does.
2. with the ip address, your machine sends a http GET request to http://videos.jacksonchen666.com
now, let's take a second to analyze the url. you see, it's sending a request to the insecure version of my videos whatever. why? idk, web browsers really shouldn't do that as a first request. but due to not all of the web having support for https (e.g. https://brainfuck.org), maybe the browsers don't.
while it's still kinda insecure (and definitely has been abused by schools or work networks to intercept requests to block or manipulate the response), it does give a response of the following:
```
HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Sat, 18 Dec 2021 11:45:15 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://videos.jacksonchen666.com/
```
the server responded with some headers, [http status code 301](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/301), including the location header. the location header along with http status code 301 tells to redirect essentially to what's defined in the location header, i.e. <https://videos.jacksonchen666.com/>, the https of my videos site.
and so the client does redirect, and now get's the following head response:
```
HTTP/2 200 
server: nginx
date: Sat, 18 Dec 2021 11:45:15 GMT
content-type: text/html; charset=utf-8
content-length: 11065
vary: Accept-Encoding
content-security-policy-report-only: default-src 'none';connect-src * data:;media-src 'self' https: blob:;font-src 'self' data:;img-src 'self' data: blob:;script-src 'self' 'unsafe-inline' 'unsafe-eval' blob:;style-src 'self' 'unsafe-inline';object-src 'none';form-action 'self';frame-ancestors 'none';base-uri 'self';manifest-src 'self';frame-src 'self';worker-src 'self' blob:;upgrade-insecure-requests
x-frame-options: DENY
tk: N
access-control-allow-origin: *
vary: Accept-Language
etag: W/"2b39-MI1GUPc3sxdOszLsp05gKxZ1t7I"
strict-transport-security: max-age=31536000
```
the version for http is now 2, the content length is longer, [http status code 200](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/301), and there's a header called `strict-transport-security` telling the client to remember to prohibit usage of http (the one over port 80) and strictly request over https, defending against attacks like [SSL stripping](https://duckduckgo.com/ssl%20stripping).
also, you're done. for that request. there are more requests for other resources, which can be requested using the same connection that was made in the first place to reduce overhead for creating a new connection.

