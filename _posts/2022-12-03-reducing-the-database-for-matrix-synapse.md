---
layout: post
title: reducing the database size for matrix synapse
author: jacksonchen666
categories:
- synapse
tags:
- matrix
- postgresql
date: 2022-12-03T14:33Z
last_modified_at: 2023-01-28T22:43:12+01:00
---
let me check on the database size of synapse...

```
postgres=# SELECT pg_size_pretty( pg_database_size( 'synapse' ) );
 pg_size_pretty
----------------
 13 GB
(1 row)
```

yeah now is probably a good time to start cleaning up the database again

# rationale for this blog post

i have found [this resource][resource] for compressing the synapse database,
but it is very outdated (used endpoints that were gone) and uses curl
requests (i'd prefer [`synadm`][synadm]). so instead, i wrote a blog post
that would take the steps outlined in that linked post, then make it useful
for people preferring `synadm` (or links to doc for `curl` users).

[resource]:https://levans.fr/shrink-synapse-database.html
[synadm]:https://github.com/JOJ0/synadm

# some notes before continuing

- for `curl` users: you are on your own. don't forget to [URL encode the `!`
  and `:` character][urlenc].
- not all of the steps are required (except for getting an access token when
  sending requests to the admin API).
- steps that directly interact with the database (or the section has the
  word "database" prefixed) should be done as the last steps.
- there are notes for each individual sections.

[urlenc]:https://en.wikipedia.org/wiki/Percent-encoding

# setting up

because this guide will use `synadm` and [synapse's admin API
endpoints][adminapi], you will need to obtain an access token to an account
on your homeserver that is an admin account.

[adminapi]:https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/index.html

with `synadm`, [this should help with setting it up][synadmsetup].

[synadmsetup]:https://github.com/JOJ0/synadm#configuration

for `curl` users: if you don't know how to get an access token, you can
~~steal~~ "borrow" an access token in your client or look at the matrix spec
for logging in to get a token ([1][loginget] [2][loginpost]).

[loginget]:https://spec.matrix.org/v1.5/client-server-api/#get_matrixclientv3login
[loginpost]:https://spec.matrix.org/v1.5/client-server-api/#post_matrixclientv3login

# finding rooms where everyone on your homeserver left it

```
synadm room list -s joined_local_members -r
```

the above command list the rooms starting from the least amount of users on
your homeserver has joined.

([list rooms admin
API](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#list-room-api),
`order_by` and `dir` parameters are relevant)

the `joined_local_members` number indicates how many users on your
homeserver is joined to that room. if it's 0, you can probably safely delete
that room without getting complaints about your homeserver leaving rooms
"randomly".

for a neat command that outputs just the room id for rooms everyone on your
homeserver left (for up to 500 rooms):

```
synadm -o json room list -s joined_local_members -r -l 500 | jq '.rooms[] | select(.joined_local_members == 0) | .room_id' | sed 's/"//g'
```
(requires `jq`, json command line processor)

# deleting rooms

now that you found some rooms that you will delete, it is time to delete
those rooms from the database.

NOTE: we do not want to block a room. blocking a room prevents joining that
room. in case you do need to unblock a room, you can use something like
`synadm room block -u '<room_id>'` for synadm. see [admin API docs for
unblocking rooms][apiunblock] if needed.

[unblockcmd]:https://github.com/JOJ0/synadm/issues/51
[apiunblock]:https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#block-room-api

here's the command for deleting a room:

```
synadm room delete '<ROOM_ID>'
```

([delete room admin
API](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#delete-room-api))

replace `<ROOM_ID>` with the room ID, and wrap it in single quotes, so that
your shell hopefully does not interfere with the `!` character (which means
something). double quotes are not usable, since it would trigger the shell
to do it's thing and change the command. or you can use escaping with
backslashes.

i deleted a few rooms, including matrix HQ and the blackarch room, because
they are pretty large and no local users was in the room.

# running state compression

with
[rust-synapse-compress-state](https://github.com/matrix-org/rust-synapse-compress-state),
state are compressed so that they take up less space.

the repository has an [automatic and simple
tool](https://github.com/matrix-org/rust-synapse-compress-state#automated-tool-synapse_auto_compressor)
to go over states and compress them. the steps for building and running the
tool has already been documented, so i won't document it again here.

# extra: purging old cached remote media

media is usually impossible to further compress. so instead, this will be
about deleting old media.

this command/admin API specifies what to delete based on the date it was
last accessed.

here's the incomplete `synadm` command:
```
synadm media purge
```

([purge remote media admin
API](https://matrix-org.github.io/synapse/latest/admin_api/media_admin_api.html#purge-remote-media-api))

**additional command arguments are required** to specify what you want to
delete. i have not included the arguments that can be specified, so you
should refer to the help information by using the `-h` argument.

# database reindexing

updates and deletes in the database can leave junk in indexes. reindexing
recreates indexes, so that it doesn't contain old junk.

([postgres routine reindexing
docs](https://www.postgresql.org/docs/current/routine-reindex.html))

## postgres >= 12

for postgres >= 12, run the following query (substituting `<database_name>`
appropriately):

```
REINDEX DATABASE CONCURRENTLY <database_name>;
```

you can run that without shutting down synapse first, as the `CONCURRENTLY`
option makes postgres lock the tables as little as possible, allowing normal
function of synapse.

[postgres 12 documentation for `REINDEX`][pg12reindex]

[pg12reindex]:https://www.postgresql.org/docs/12/sql-reindex.html

## postgres 11

concurrent reindexing is not an option in postgres 11, so **you should
stop synapse before running `REINDEX` on the synapse database**. the SQL
command would look like this:

```
REINDEX DATABASE <database_name>;
```

---

reindexing made the database size 3GB smaller.

[postgres 11 documentation for `REINDEX`][pg11reindex]

[pg11reindex]:https://www.postgresql.org/docs/11/sql-reindex.html

# database vacuuming

NOTE: a `FULL` vacuum can prevent synapse from working and requires extra
disk space, so **stop synapse and make sure you have enough free space before
running a full vacuum**. if that cannot happen, remove `FULL` from the SQL
statement for at least, some vacuuming of the database.

NOTE: **ensure you're connected to the correct database.** else, you'll
vacuum the wrong one.

```
VACUUM FULL;
```

([postgres 11 documentation for `VACUUM`][pg11vacuum])

[pg11vacuum]:https://www.postgresql.org/docs/11/sql-vacuum.html

## vacuuming stats

before vacuuming (after reindexing):

```
synapse=# SELECT pg_database_size( 'synapse' );
 pg_database_size
------------------
      10654737187
(1 row)
```

(10.6 GB)

anyways, after all this vacuuming mess:

```
synapse=# SELECT pg_database_size( 'synapse' );
 pg_database_size
------------------
       5637989155
(1 row)
```

(5.6 GB)

```
synapse=# SELECT pg_size_pretty( pg_database_size( 'synapse' ) );
 pg_size_pretty
----------------
 5377 MB
(1 row)
```

(numbers might be inaccurate due to incorrect labeling of units)

---

anyways, hope that helped, even in the future. if something doesn't work and
you followed the guide as intended, [please throw an email at me]({% link
contact.md %}).
