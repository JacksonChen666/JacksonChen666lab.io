---
layout: post
title: analyzing the result of tracking google searches without using google analytics on my website
author: jacksonchen666
categories: [search]
tags: [google search console]
date: 2022-05-24T22:12:12+02:00
---
"google bad", at least that's what they say.
they say it because it's bad for your privacy and stuff like that.
however, not everyone switched away from google yet (only counting direct users[^1]).
google also provides what they call [google search console][gsc wiki] which provides search analytics from the google search engine.
it does not require placing a google provided tracker on your website (the most google on my website is a site verification tag, which also is just a meta html tag).
so, what interesting data can i find in there?

oh and yes, i have a google account.
that's because i have a youtube channel, and that requires a google account.
and so, using the opportunity of having a google account, i added my domain to the google search console (in like 2021).

# the data - most clicked link to my website
i think it is [this post]({% post_url 2021-04-03-making-and-building-custom-arch-linux-isos %}), from the search query "how to make a custom arch iso", "arch linux custom iso", and "custom arch iso".
totaling 6 clicks and 130 impressions, from web searches, within all of the data.
not really impressive, but this is basically a random website (and i still haven't figured out how to grow to be more visible).

## the minor problem reminder - search engine exclusivity
to remind you again,
the data that i got from google search console is *pretty exclusive* to people using the google search engine (like google trends).
again, other search engines do exist, and not 100% of the people who have access to the internet use google (mostly because they switched away from it).

# coverage data - correlation with ssd upgrade
google also has coverage data on the coverage of indexing a website.
interestingly, the rate of errors dropped noticeably after the [server ssd upgrade](https://status.jacksonchen666.com/issues/2022-04-04-storage-upgrade-ssd/#update-2022-04-09t2335400200---it-is-now-working-and-everything-has-been-checked), probably because things are faster.

# that is all
all of this is done i guess

[gsc wiki]: https://en.wikipedia.org/wiki/Google_Search_Console
[google search console]: https://search.google.com/search-console

[^1]: i'm defining a direct user of a thing (like google search) as a person who uses the thing (google search) directly (through google.com) and not indirectly (something like a meta search engine using google)
