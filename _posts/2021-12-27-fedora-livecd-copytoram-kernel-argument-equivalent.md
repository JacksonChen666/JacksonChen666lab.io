---
title: fedora livecd copytoram kernel argument equivalent
author: jacksonchen666
date: 2021-12-27 17:51:59 +0100
categories: [linux]
tags:
- fedora linux
- copytoram
migrated: true
---
the exact title of this post yielded no useful results. however, this post should become one of many useful results that could be buried away.

let's say you want to use the fedora livecd and boot off from a dvd (who cares if it's almost end of 2021), but need to copy the image to ram because you need to unplug the dvd drive to free a usb spot. but the kernel argument `copytoram` unfortunately doesn't work. fortunately, i accidentally stumbled upon a man page for `dracut.cmdline(7)` which actually contained useful information, including how to enable the `copytoram` equivalent, which is `rd.live.ram=1`.

adding that to the kernel argument should do the equivalent of `copytoram` that i have done in arch linux. basically, read and find man pages too, not just forums and internet resources.
