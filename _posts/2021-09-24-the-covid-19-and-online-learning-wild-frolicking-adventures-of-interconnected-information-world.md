---
title: The COVID-19 and online learning - Wild Frolicking Adventures of Interconnected
  Information World
author: jacksonchen666
tags:
- google trends
categories: [wild frolicking adventures of interconnected information world]
date: 2021-09-24 22:23 +0000
last_modified_at: 2021-10-10 21:15:12 +0200
---
Welcome to a potential series of: Wild [Frolicking](https://en.wiktionary.org/wiki/frolic#Verb) Adventures of Interconnected Information World, where I explore the internet to show you maybe interesting things.
The title was inspired by bill wurtz video called [Wild Frolicking Adventures of Informational Education](https://youtu.be/1PDQ22sePmU), in which he rambles with stuff related in anything on a map.

This time, it's about the peak of online learning (or, google classroom, actually).

Due to the people who are unfortunately using google as a search engine, their search query has been recorded to their accounts, and published anonymously in a grouped form (google trends).
This means that anyone on <https://trends.google.com/trends/?geo=US> can compare how trendy is a search query or topic (either by itself or compared to another topic), in what country and in which years, which google records (and that's the reason I don't use google as a search engine).

Because of the [COVID-19](https://en.wikipedia.org/wiki/Covid19), schools were closed and lock downs happened. Now children can't learn because school is not available. Or is it?

Online learning suddenly became a thing, and well, it's real.
It's that you're at home pretending to be at school while [your teachers communicate with you over non-free software](https://www.gnu.org/education/education.html)[^1] because yOu CaN't HaVe nOtHiNg To Do!1!!![^2]
And what are the non-free software[^1] that schools use? Of course [Google Classroom](https://en.wikipedia.org/wiki/Google_Classroom). What did you expect?

Finally, enough context for people who managed to ignore the fact the world dying. Now on to the graph or something.

# The interesting part
![A graph on google trends, showing the worldwide interest for "google classroom" as a search term, peaking at april of 2020. The graph ranges from the first feburary of 2014 to the 24th of september of 2021.](/assets/images/posts/wild-frolicking-adventures-of-interconnected-information-world/google-classroom-peak/peak-google-classrooom.png)

OK, so, from this alone, yeah there definitely is online learning happening.

## What's more? Telling when online learning starts?
Sure, let's try that. Why not.

![Graph on google trends, same as last image, but this time, the data is based on Bolivia, peaking between March 15 and 21 of 2020.](/assets/images/posts/wild-frolicking-adventures-of-interconnected-information-world/google-classroom-peak/bolivia-rise.png)

So, between March of 15 and 21 of 2020. That's likely the date of lockdown for Bolivia, a random country that I picked.

# Oopsies, end.
End.
That's the end.
There is no more.
I'll go fix firefox reader mode going too far and taking my more posts stuff.

# Footnotes. No, it doesn't go onto the foot-
[^1]: Free as in freedom. See [GNU philosophy](https://www.gnu.org/philosophy/philosophy.html).
[^2]: said Jackson's subconscious brain
