---
layout: post
title: history of sr.ht (the domain, not sourcehut)
date: 2023-01-07T15:43:25+01:00
---
did you know sr.ht was originally a file hosting service? well, [here's the
code for (legacy.)sr.ht](https://git.sr.ht/~sircmpwn/legacy.sr.ht) before it
became [the sourcehut we know and love
today](https://sourcehut.org).

as this post is about the history of the sr.ht domain name, here's the list
of events related to sr.ht:
- 2015-02-23T16:29:10.748Z: (whois database) sr.ht domain registered
- 2015-06-09: (web archive) [first snapshot of (legacy.)sr.ht][walsrht1]
- 2015-06-09T17:24:28Z: [(legacy.)sr.ht gets it's first commit][lsrhtinit]
- 2015-09-16: ((web archive) [snapshot of (legacy.)sr.ht][walsrht2])
- 2016-01-15: ((web archive) [snapshot of (legacy.)sr.ht][walsrht3])
- 2016-02-11: ((web archive) [snapshot of (legacy.)sr.ht][walsrht4])
- 2016-03-27: (web archive) [last known snapshot of (legacy.)sr.ht][walsrht5]
- 2017-03-07?: [file hosting announced within update of blog post][hostpost]
  (see bottom of post, no useful earlier reference from [git
  history][bloginitlist])
- 2017-10-14: (web archive) [sr.ht is now closed alpha sourcehut][wasrht]
  ([destination][wasrhtclosedalpha])
- 2018-11-15: [sourcehut open alpha announcement][sourcehutopen]

  at that point, what was previously sr.ht (for file hosting) now seems to
  live on both <https://l.sr.ht> and <https://legacy.sr.ht>, so they still
  can serve files from long ago on blog posts (including Drew's own
  website).
- 2018-12-17: (web archive) [(meta) sourcehut is open for registration][wasrhtopenedalpha]

if you have anymore information that you can reference, please [contact
me](/contact).

the work to content ratio wasn't the greatest, but hey, it's not obvious
that there was something else before sr.ht unless you read way too many of
[Drew's blog posts][drewblog].

[bloginitlist]:https://git.sr.ht/~sircmpwn/drewdevault.com/log/master?from=7f3fc813e85c30b6a45dd52539cd934c28fb90ac#log-a00d4ff679113d1ef9ad84ed3ee5d95d42d8ec8a
[drewblog]:https://drewdevault.com
[hostpost]:https://drewdevault.com/2014/10/10/The-profitability-of-online-services.html
[lsrhtinit]:https://git.sr.ht/~sircmpwn/legacy.sr.ht/commit/24bd8440a6d35d0dee362e59c765f2e8b97b7b58
[sourcehutopen]:https://drewdevault.com/2018/11/15/sr.ht-general-availability.html
[walsrht5]:https://web.archive.org/web/20160327011442/https://sr.ht/
[wasrht]:https://web.archive.org/web/20171014174334/https://sr.ht/
[wasrhtclosedalpha]:https://web.archive.org/web/20171028162947/https://meta.sr.ht/
[wasrhtopenedalpha]:https://web.archive.org/web/20181217100209/https://meta.sr.ht/
[walsrht1]:https://web.archive.org/web/20150609180204/http://sr.ht/
[walsrht2]:https://web.archive.org/web/20150916172940/https://sr.ht/
[walsrht3]:https://web.archive.org/web/20160115210015/https://sr.ht/
[walsrht4]:https://web.archive.org/web/20160211154522/https://sr.ht/

<!--
oh hey, it's a comment!
abbreviations (for markdown links stuff):
wa: web archive
lsrht: l.sr.ht or legacy.sr.ht
init: (probably initial commit)
-->
