---
migrated: true
title: a one liner for mouting necessary file systems for chrooting in gentoo
date: 2021-12-22 21:17 +0100
author: jacksonchen6666
categories: [commands]
tags:
- chrooting
- gentoo
---
ok hi gentoo users or people from [here](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Base#Mounting_the_necessary_filesystems) or people who stumbled upon this while trying to chroot in linux but needed to mount necessary file systems, wonder how to do that 7 commands into one? well here:

`for i in sys dev run; do mount --rbind /$i /mnt/gentoo/$i; mount --make-slave /mnt/gentoo/$i; done; mount --types proc /proc /mnt/gentoo/proc`

# how i reduced it
in void linux somewhere, i found something similar. so i applied what i seen in void linux and then applied it to gentoo linux install guide, making the pain of copy pasting 7 commands into one, which still runs 7 commands. it is less painful to copy one rather than 7.
