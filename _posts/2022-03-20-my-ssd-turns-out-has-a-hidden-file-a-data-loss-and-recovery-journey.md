---
layout: post
title: my ssd turns out has a hidden file - a data loss and recovery journey
author: jacksonchen666
categories: [data recovery]
date: 2022-03-20 17:21:03 +0100
migrated: true
---
well, this data lost isn't recent. it was on 2021/04/20 (and no, it's not nice), and i don't even know when i will actually need the files that i lost, nor do i remember anything that was lost probably due to the importance of not really (also the purpose of my ssd being mostly archival and not work since i got a not-the-base-model storage size of 500GB of internal storage, pro tip: get macs with just a bit more storage because base models storage sizes are a bit too small for some needs including software development tools and macOS updates).

# the data loss and the cause
i think the cause of it was negligence, because of what i did to delete the entire folder was actually irreversible, similar to the `rm` command but done in finder.

the shortcut was (try it at your own risk): command-alt-backspace, which shows a prompt basically saying "are you sure you just want to delete it and not move it to the trash bin?"
(fun fact: you can move to trash with command-backspace when a file/folder is selected, and empty the trash with command-shift-backspace (finder only, not global shortcut) or with the additional alt key to empty trash without prompt (which may cause data loss if you're negligent))
yeah, i did command-alt-backspace, didn't read the prompt, didn't realize what i was deleting, and then it deleted it. just gone.

and then i realized and was like "oh crap"

# the data recovery journey
data recovery was, not successful, due to APFS encryption not being as interactable as LUKS on linux where you can read data in the form of luks header + encrypted data or the file system unencrypted. all you get on macos is transparent encryption for programs, and basically no way to access directly the partitions unencrypted form, which can be useful for cases like data recovery.

i was in no way going to use proprietary software, so i used some open source software. result, nothing. attempt 2, whole disk, nothing but a few. and the few? a random user guide PDF about my ssd.

how? probably because the formatting procedure managed to not touch where the file was, or it was kinda just never visible to the user.
