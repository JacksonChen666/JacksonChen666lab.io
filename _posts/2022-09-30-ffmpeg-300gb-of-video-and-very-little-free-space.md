---
title: ffmpeg, 300GB of video, and very little free space (relatively)
author: jacksonchen666
tags: [large stuff, ffmpeg]
category: [ffmpeg]
date: 2022-09-30T19:11:04+02:00
---
# the problem
so, i have 300 GiB of video on an external SSD which currently has 178 GB of free space left (not a lot of space left when the storage size is 2TB).
to be precise, ~300 gibibytes of recorded gameplay of me playing roblox jailbreak (uploaded to my youtube channel (may or may not exist anymore, i can't guess what i will do in the future)) so i have some evidence to say that i'm not cheating (footage does not include times before i started doing this practice).
my other recorded videos take up ~40GB after processing them with ffmpeg.

that makes my jailbreak footage seem unreasonably large, and so i want to re-encode them.
however, they have been already encoded, twice in a lossy manner (1 at OBS at ultrafast, 2 is a re-encode after recording with OBS). so now i'm going to have to process them even more, and this time it is complicated than just passing an input file name and output file name to ffmpeg.

here's the sizes and options as it goes down the chain of video reprocessing:
1. OBS, ultrafast setting for ffmpeg, relative size is ~2.0 (usually ~30GB-60GB)
2. ffmpeg lazy re-encoding, no extra options specified, relative size is 1.0 (the baseline size)
3. ffmpeg but with much data loss, resolution sliced by half (baseline is 3360x2100) and 30 FPS, relative size is ~0.3

(relative sizes are averages)

so, given the third re-encoding, it should reduce the sizes of the videos by a lot, which means more free space.

now the question is, how did i do it?

# ffmpeg options
- the most basic of all, input file name is given to the `-i` argument and the output file name is given to no arguments.

  `ffmpeg -i input.mkv output.mkv`
  
  that is the most basic command you can do with ffmpeg. just encode the video and everything at defaults.

- set the FPS to 30:

  `ffmpeg -i input.mkv -r 30 output.mkv`
  
  output options must be specified before the output file name. same rule for input options and input file name.

- setting the resolution to half of 3360x2100 and keeping aspect ratio (with the [scale video filter]):

  `ffmpeg -i input.mkv -r 30 -filter -vf scale=1680:-2 output.mkv`
  
  the `-2` in the height part specifies the size dynamically and follows these rules (taken from [ffmpeg docs][scale video filter]):
  - sets a value to keep aspect ratio
  - make sure the result is divisible, adjust if necessary (useful for when codecs complain about an odd number of pixels specified)

## the first test subject
i re-encoded the first gameplay video with the options mentioned above

reviewing the video that was re-encoded for the third time and comparing it to the video encoded twice, the quality of halving the resolution is noticeable but not unusable. going from 60 FPS to 30 FPS is not much of a loss, but most videos are 30 FPS because OBS just could not handle the large amount of changes in the video (third person, user rotatable camera, full screen video game).

# the aftermath
after spending a lot of time encoding videos and also deleting the previous videos, the size of the gameplay videos went from around 300GiB to 100GiB.

i'd say that was a success of trying to use less space.

[scale video filter]: https://ffmpeg.org/ffmpeg-filters.html#scale-1
