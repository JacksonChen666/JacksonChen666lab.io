---
title: detailing the internal workings of fstransform by guesswork
author: jacksonchen666
date: 2021-11-29 15:28:51 +0100
categories: [file systems]
last_modified_at: 2022-03-29 17:26:37 +0200
---
ok so, i decided to transform the server's root file system from xfs to ext4 because shrinking with xfs isn't so possible (i heard it's planned) and performance is not what i am aiming right now.
the transformation was successful and no data seems to have been lost, except for selinux file context metadata (fixed by [`touch /.autorelabel`][nspd] which resets all file contexts. also fixed [no shell: Permission Denied][nspd] upon login)

to start with the explanation of the tool, fstransform is a tool that can turn supported file systems into another supported file system. like in my example, xfs to ext4.

to start, you must compile the tool.
that's right, step 0 is the first step. in case you're wondering what's needed to compile (in arch packages), i believe it's:
- gcc
- make
- autoconf

needed to compile the tool.

the file system you're trying to transform cannot be in use, must be mountable and unmountable, be supported (to be safe), and have at least *some* free space (5% free space, which then you're on the edge with a potential to fall into data corruption).

then run the main tool, tell it which device, and which file system to transform to. it will start doing checks to see if things are working, tell you to do a thing (so do the thing), and then start fsmove.

now, this is the guesswork part. how fsmove works is:
1. create a sparse file on the old <abbr title="file system">fs</abbr> (pure guessing)
2. formats the sparse file with the new fs (pure guessing)
3. move everything file by file into the new fs (evidence is files are being "deleted" from the old fs)

then the part that i haven't figured out at all: fsremap. seems to remap from old fs to new fs, idk.

and the final big part: clearing the... free space? i don't know what even happens, as there's minimal documentation (or none at all) and i have barely read through the code.

after it does that, it does a <abbr title="file system check">fsck</abbr> and is complete.

[nspd]: https://forums.fedoraforum.org/showthread.php?209255-login-no-shell-permission-denied&p=1140339#post1140339
