---
migrated: true
title: wildcard certificate with certbot and lets encrypt
author: jacksonchen666
date: 2021-12-21 12:31 +0100
categories: [certificates]
tags:
- certbot
- wildcard certificates
---

after having way too many certificates to be responsible with, i've decided to introduce a single point of failure with a higher risk of being fucked over by being compromised and also bringing some convenience to dns and certificates, with the convenience of not having to get another certificate and dns record for a new thing.
also, [wildcard certificates do have caveats and isn't a wildcard to every possible subdomains](https://certbot.eff.org/glossary#wildcard-certificate)

# how it's done
i used [certbot](https://certbot.eff.org) to get a certificate.
to get a wildcard certificate, verification of ownership must be done via TXT dns records, manually (not recommended for automatic renewal) or automatically (with a plugin for your dns or with your own script).
getting a wildcard certificates also seems to require a newer version of the server, the version 2 of acme.

resources:
[instructions for certbot](https://certbot.eff.org/instructions?ws=nginx&os=fedora&commit=%3E), [forum post about wildcard certificates](https://community.letsencrypt.org/t/getting-wildcard-certificates-with-certbot/56285), [documentation on dns plugins](https://eff-certbot.readthedocs.io/en/stable/using.html#dns-plugins), [gandi dns plugin](https://github.com/obynio/certbot-plugin-gandi)

command (for my case): `certbot -a dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi/gandi.ini --server https://acme-v02.api.letsencrypt.org/directory -d \*.http3.jacksonchen666.com -i nginx`

# how much work reduced
- no need to create dns record on new subdomain
- no need to get certificate on new subdomain

# risks
- compromised certificate allowing any subdomain

# responsibilities, but before
dns records, certificates

# responsibilities, but after
certificates
