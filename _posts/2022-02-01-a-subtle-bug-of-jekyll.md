---
layout: post
title: a subtle 'bug' of jekyll
author: jacksonchen666
categories:
- jekyll
tags:
- weird behavior
date: 2022-02-01 06:18 +0000
---
you might be wondering what's the 'bug' all about. well, it starts with [me having a power outage that effected my services](https://status.jacksonchen666.com/issues/2022-02-01-power-outage/).

well, i'll have to show you through pictures, once again. the new blog is dead from the power outage (because it depends on my server).

![figure 1: live website showing 404 error page on the url /maintenance/](/assets/images/posts/subtle-bug/live.png)
<sup>figure 1: live website</sup>

![figure 2: local website showing the posts in the maintenance section](/assets/images/posts/subtle-bug/local.png)
<sup>figure 2: local website</sup>

as you can see with figure 1, it's a not found error.
with figure 2 though, it's actually what i expected.

and this is where the quotes around 'bug' comes in

# differences between gitlab pages and local development webserver by jekyll
jekyll uses a webserver that's in... ruby i think, and it can handle extensionless urls.
with gitlab, the behavior is ever so *slightly* different.

i had to go to my website and append `.html` to the url just to make it work. why exactly?
well, it probably has to do with the way i handled permalinks for that page.
you should be aware that the file isn't a file called `index.html` within a folder called `maintenance`, so the webserver wouldn't even find the file. right?

no, that doesn't make sense.

the webserver should append the `.html` extension to find files locally and it does that and there is a file called `maintenance.html` which should've been served, right?

well, i just checked `_site` and realized there are multiple things called `maintenance`:
- a folder containing posts
- a page listing the posts

yep, name collusion or the server prioritizing the folder over the file seemed weird.
