---
title: info about my email address
date: 2022-01-06 21:09:14 +0100
categories: [website]
tags:
- contact
author: jacksonchen666
pinned: false
migrated: true
last_modified_at: 2022-08-24T18:54:22+02:00
---
this post is about how i think you should refer to my email/contacts.
yes, i think you should do it this way. because i don't put my real email address up front available.

but let's say you also write stuff. maybe you talk about contacting me (for some reason). but you should consider *not* copy pasting the email that's currently on the main site. it can change, and it will when needed, and the old email will die in silence, all without notice.
your best bet of linking people to my contacts is <https://jacksonchen666.com/contact/>
