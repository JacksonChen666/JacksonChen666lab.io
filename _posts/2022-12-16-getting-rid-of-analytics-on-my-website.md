---
layout: post
title: getting rid of analytics on my website
author: jacksonchen666
categories:
- website
date: 2022-12-16 00:29 +0000
---
today i decided to remove plausible analytics from my website.

and [here's when i introduced it]({% link _posts/2022-06-09-now-getting-useful-analytics-on-this-website.md %}).

# motivation
i was influenced by an [email provider that removed analytics because they concluded that it was no use](https://www.migadu.com/blog/redesign/#no-analytics-or-tracking).
that post made me reflect on whether it is useful for me to have analytical data.

## lack of popularity (at least, my website)
unfortunately (or fortunately), i am *very* unpopular.
i'm about the same as every other person you might see in some chat rooms or IRC channels.

## upgrade pain
the open source plausible is sort of a LTS version[^1], and i think i started on version 1.4.4.
after 1.5.0 was released, [dumb things happened](https://status.jacksonchen666.com/issues/2022-12-03-tracking-is-down/) and i had to fix a stupid mistake of mine.
however, version 1.5.1 was also very difficult to upgrade to because it would never startup (for some reason).

# action taken
i have removed all plausible and analytics related things, including:
- the snippet
- the opt out page
- some text from the privacy policy page

though, i have not done everything, like:
- yeet plausible

i haven't done the above because i would be throwing away data.
thing is, i already have some data, except it's just some and maybe not very useful.

# conclusion
what conclusion?
it's 1 AM.
my eyes are tired.

anyways, enjoy the footnotes.

---

[^1]: <https://plausible.io/docs/self-hosting#3-updating-plausible>
