---
layout: post
title: completely getting rid of submissions, and the history of submissions
author: jacksonchen666
date: 2022-11-19T21:00:26+01:00
categories: [submissions]
tags: [website]
redirect_from:
- /questions/
- /ideas/
- /feedback/
- /more-questions/
- /more-ideas/
---
today, i decided to get rid of submissions (ideas, questions, feedback, etc.).

# reason i did it
1. it has attracted more spam than non-spam (in terms of size of files)
   
   i have found a solution to filtering the spam, because most of the spam that was common had either contained an email address or a phone number. links are also probably included.
   but the signal to noise ratio was still pretty bad (high noise), and the only solution to prevent the spam submissions from happening at all was to [make it more difficult for everyone]({% link _posts/2021-11-17-why-i-made-the-submissions-defunct-to-the-clearnet.md %}).
2. it required a server, while my website didn't really need one (ok, one to serve the contents is one, but it doesn't accept user data nor am i responsible for it)
3. it hinders my ability to carelessly apply a license to the entire website (i.e. i must selectively license parts of my website under something like CC-BY 4.0 as i didn't write the questions!)
4. it has been dead

# reason i started in the first place
1. because [bill wurtz did it](https://billwurtz.com/questions/questions.html)
2. (potentially a reason i made up on the spot) i don't know how much questions i will get, but it could be a lot if i keep it up

# history
## when it started
the problem is, nothing was recorded about the questions page on exactly when it was started.

all i know is, it was created between the time i created my website (2020-02-26, from the deleted `memes.md` file), and the time i dumped my entire website into my git repo (2020-03-05, commit sha1: `59aea919469fb9380a3dd8400cc827a43473d99b`).
in between that time period, there is no record in the git repo. because i didn't start my website with git recording all my changes throughout.

## (minimal) changes
i gotta say, the questions page hasn't fundamentally changed since the beginning of my website compared to pretty much everything else.
heck, it is the only thing that lasted from 2020 until today. (other than, well, the existence of my website)

i mean, look at today[^newref]:
![questions page before it was removed](/assets/images/posts/submissions-yeet/new.png)

and compare that to 2020[^oldref]:
![questions page in it's earliest form](/assets/images/posts/submissions-yeet/old.png)

what has changed?
- the size of the box
- colors
- the rest of the website in general

but the fundamentals have not changed. which are:
- a textbox
- submit button
- questions answered

those fundamentals have not changed, since i dumped my website into the git repo.
like ever.
i don't think there's anything else on my website that hasn't changed at the fundamental level since i dumped my website into the git repo, and now there's nothing else.

---
i don't think i have anymore to say.
anyways, enjoy the footnotes

---

[^newref]: `questions.md` at commit `6a67b9ad4beafb9b79203930b05e682dd54a7ac1`
[^oldref]: `development/questions.html` at commit `59aea919469fb9380a3dd8400cc827a43473d99b`
