---
title: Vanity onion address shenanigans
categories: [tor]
tags: [onion address]
date: 2021-08-19 00:30:15 +0200
author: jacksonchen666
last_modified_at: 2022-03-23 21:55:18 +0100
---
Well, [Tor][tor] is a thing, and I have to say: yeah it seems pretty cool but probably not gonna daily drive that.
But with Tor, does it have [onion services][onion services], which is hidden ~~from any of the public prying eyes.
By hidden, I mean, there's like literally no way to stumble on an onion site on accident (unless you're extremely lucky) without being told by anyone or anywhere.~~[^4]

# Tor Onion services and addresses[^2]
## Facebook, the inspiration for me generating vanity onion addresses
If you know or don't know Facebook, well, it doesn't even matter.

Facebook themselves have a vanity onion address (both version 2[^1] and version 3), and theirs is [facebookcorewwwi.onion][facebook onion v2].

However, their version 2 onion address is memorable and pretty cool, and they made it the [backronym of *"Facebook's Core WWW Infrastructure"*](https://en.wikipedia.org/wiki/Facebook_onion_address#History)

## Generating vanity onion addresses
To generate vanity onion addresses, I used [this tool][tool] and then tried some filters like "jacksonchen666", "jc666", "jack666", "jackc666", "jackchen666", "jackson666", but I've only got results from "jc666" so far (I can do about 7 million calculations per second on my mac, but the max calculations maybe needed is 32\*\*L, with L being the length of the filter).

I should also mention that the tool I used, is basically brute force until something matches, so choosing a filter

The tool is available on all platforms, but requires the command line (with some assembly required[^3])

### Random onion addresses shenanigans
I've got some onion addresses generated.
Actually, 7197 of them.
Most of them are not the most interesting thing, so I just stored it away.
An Asterisk (\*) means that any amount of any characters is in between.

A list of onion addresses that I found interesting:
- jc666\*666\*mad.onion
- jc666\*666\*dad.onion
- jc666y666
- jc666pgp\*ddd\*666 (that's a lotta triple characters)
- jc666\*dos
- jc666usmozg\*dos (among us)
- jc666oosupiud\*666 (ooo stupid)

Some random other discoveries
- While generating random onion addresses, I found out that all version 3 onion addresses would end in "d.onion" instead of just ".onion"

## Redirecting Tor users
I might release the onion address later quietly, and it will only be available through [HTML meta tags](https://community.torproject.org/onion-services/advanced/onion-location/#using-an-html-%3Cmeta%3E-attribute) because I don't have control over the HTTP headers of the server that hosts my website (GitLab.com hosted).

If you're using Tor, you should be able to upgrade from my normal domain to an onion address when I add the meta tag.

Or you can [find it on my mirrors page](/mirrors).

### Advice for Tor users
Bookmark all onion services you frequently use.
Or really, any of them you'll revisit.

Though partially quick checking isn't a good idea

# End and footnotes
I hope you enjoyed reading text, and this time I'm trying a new structure and stuff.

[^1]: [Version 2 of onion services is being deprecated][onion service version 2 deprecation]
[^2]: I’m assuming that you know what the things I mention are. If you don’t, search it up on the internet. [But here's a page talking about onions](https://community.torproject.org/onion-services/talk/)
[^3]: Some assembly required, actually meaning you'll have to configure then compile the tool. Performance fine tuning is also possible.
[^4]: I have not done any research about such thing. You should do your own.

[tor]: https://www.torproject.org/
[onion services]: https://community.torproject.org/onion-services/
[tool]: https://github.com/cathugger/mkp224o
[onion service version 2 deprecation]: https://support.torproject.org/onionservices/v2-deprecation/
[facebook onion v2]: https://facebookcorewwwi.onion/
[facebook onion v3]: https://facebookwkhpilnemxj7asaniu7vnjjbiltxjqhye3mhbshg7kx5tfyd.onion/
