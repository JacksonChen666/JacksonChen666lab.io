---
title: How to accidentally create a bot check without utilizing it
author: jacksonchen666
categories:
- website
- submissions
tags:
- spam
date: 2021-10-10 15:16 +0000
last_modified_at: 2021-10-10 21:17:06 +0200
---
So, I've had to deal with spam from submissions on a website that I don't think anyone truly engages with.
How much spam? More than all the user contributions combined (or about 30kb now in spam).

To deal with the spam, I disallowed URLs and email addresses (a common pattern between all the spammers) and that worked actually greatly: About 100% of the spam I received was redirected to a central file that's named spam, and includes a lot more details about the submission itself (I will not make any spam public, or even include it. I'm just going to keep it or just dump it).

And then there's this small oversight that I've made, below what you'll see:

![A picture of the of the questions page]({% link /assets/images/posts/small-oversight/questions.png %})

It's not clear, isn't it? <u title="The image itself is transparent">(no pun intended)</u>

Well, it's the "ASK" button.
That's the oversight.
Right in front of my face.

# "But how is that an oversight?"
Well, it is definitely not in plain sight.
And when it's not in plain sight, it can become an oversight.

So, starting with an issue with the [submissions php script having 10/16 failed test cases](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipelines/377773634/test_report) (first case of failure).
I tried to debug [the issue](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues/17), found a stupid mistake of using backticks instead of normal quotes.
I fixed the stupid mistake, and got the [failures down to 3 test failures](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipelines/385479115/test_report).

It is in between did I discover the oversight: the POST request

![A POST request detailing the data sent including the formSubmit data, which is not used for anything]({% link /assets/images/posts/small-oversight/post-request.png %})

I was seeing what was sent, and to my surprise, it included the formSubmit value which I actually never read in the submission receive script.
That was the button itself.

# What I'll do with such information
Use it to my advantage a create a possible spam filter again, because of the side affect of the "ASK" button on the [questions page](/questions/).

I'm going to see if it actually works in the real world (which I haven't yet), and then implement a test case anyways lol.
