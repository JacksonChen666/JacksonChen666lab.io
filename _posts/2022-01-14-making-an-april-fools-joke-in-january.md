---
title: making an April fools joke in January. for April fools.
author: jacksonchen666
layout: post
migrated: true
categories: [april fools, website]
date: 2022-01-14 23:25:36 +0100
last_modified_at: 2022-08-24T18:30:26+02:00
---

today i decided to have an idea: make an april fools joke that'll self activate and deactivate without any manual intervention.

first, let me review-
# what i could learn from this
- how to turn text upside down in css (that's the idea)
- scheduling pipelines in gitlab

# how it can be done
gitlab ci (used to build and deploy my main site/old blog) pipelines can be scheduled with ([c-][c-cron])cron syntax and specific timezones, and then i could make the thing in a separate branch then later merge it when it's done. it's changes won't interfere with anything existing (unless enabled), has sensible defaults (for the new options), and that's about it.

# limitations
i dunno

# the process
this post was written while i was in the progress of making this joke

## planning
0. create an issue that tracks progress
1. make 2 scheduled pipelines
2. turn the text "Jackson Chen" upside down, not by letter but the entire thing
3. test that nothing changes if the option isn't enabled
4. test that something changes if the option *is* enabled

## scheduling the pipeline
i went to the [repo][website repo], [scheduled 2 pipelines][pipeline schedules], both in -0600 timezones (because US speak english and i have lot of english stuff so target US with pure guessing) and scheduled it 5 minutes before april fools to make it already happen on april fools and not have to wait for like 2-3 minutes to build & deploy.
also, the accuracy of gitlab.com ci scheduled pipelines is documented to be 5 minutes (somewhere).

## making the thing
welp, time to start making the thing.
hmmm, css transform?
yep, transform with rotate works as expected, mostly. the text is now on the other side. i want it in place, not over there.
trying putting thing in span and rotate... nothing happens.
css text align right text?
that works, except now "en" (upside down) replaces "./".
if i comment out [css 81-84][css 81-84], the "./" is gone entirely and still moved a bit too far. hmmm
maybe i could try devtools instead. hmmm, negative margin? ah! found it. [line 69][css 69], nice.
and if i override it with later declarations, and it's nice. the "./" is just somewhat out of place, but i guess that's the best i can make it.

## flipping it upside down - on option
luckily, the css file was under jekyll's control with if statements maybe.
however, it completely breaks all linting. fun.
added if statement, the thing returns to normal. because it's checking `ENV.UPSIDE_DOWN_TITLE` and i forgot where to get env vars.
referring to [\_includes/post\_text.md][post text ref], it's `site.env`. cool.
change it accordingly, it's done.

## testing without option
works as expected.
serve a production site, also works.

# what about the tor site?
we do a *little* trolling, not *too* much.

# conclusion
with only [6 insertions][ap commit], [2 scheduled pipelines][pipeline schedules], [one tracking issue][tracking issue], [2 failed][cmd fail 1] [commands][cmd fail 2], 41 minutes of tracked time, [one branch][ap branch], [one blog post]({{ '/posts/2022-01-14/22-25-36' | relative_url }}), 13 unique links in this post, at least 2 chicken and egg problems already resolved, more than 600 words in this post and 1 joke that'll appear on april fools without any human intervention which doesn't apply to the new blog and such change that'll be somewhat noticeable and probably infuriating for some, there you have it.

an automatic April fools joke, made in January.

have a great day.

[c-cron]: https://micro.jacksonchen666.com/@jacksonchen666/107621831949657396
[gitlab ci schedule]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[gitlab ci documentation]: https://docs.gitlab.com/ee/ci/
[website repo]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io
[pipeline schedules]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipeline_schedules
[tracking issue]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues/19
[css 81-84]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/blob/491e61fa5490a814b01ef569273684c2ac0aa2b6/assets/css/style.scss#L81-84
[css 69]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/blob/491e61fa5490a814b01ef569273684c2ac0aa2b6/assets/css/style.scss#L69
[post text ref]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/blob/491e61fa5490a814b01ef569273684c2ac0aa2b6/_includes/post_text.md#L2
[ap branch]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/tree/april-fools
[ap commit]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/commit/8625b2a245b0400a190aebf95981bad3c218cdd6
[cmd fail 1]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues/19#note_811363213
[cmd fail 2]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues/19#note_811381505
