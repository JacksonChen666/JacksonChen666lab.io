---
layout: post
title: linux high availability networking with bonding
categories:
- linux
- networking
tags:
- bonding
- link aggregation
author: jacksonchen666
date: 2022-06-02 19:47 +0000
---
hey so the server [had some downtime](https://status.jacksonchen666.com/issues/2022-06-01-server-down-just-straight-up-down/) because some person took out the ethernet from the server (the one that hosts subdomain services) for their use because their wifi didn't work (q of day: did they bother with fixing their wifi? no idea).
to make sure the result of that doesn't happen again, i've decided to start to introduce some internet connection redundancy.
with [bonding].

# the event of the problem
well, to recap, server had downtime because ethernet was disconnected.

there was no backup connection the server has setup (yet), and so it went offline without notice.

and i noticed that the server was ~~up~~ down to something when my vpn connection to the server stopped working.

i suspected it was a complete network failure (or router go offline).
and to figure out if it was or not, i tried accessing it over tor. no luck.

i also try to access another server that i had that was also going over onion services, but in reality it was completely the wrong onion address.

after arriving, i discover it was disconnected. and so i fix that issue.

but the problem is the next time it will happen.

# the solution
the solution is to use network [bonding].

## what is network bonding?
it is where multiple network connections are established, and the bond could act as a network connection with redundancy under the hood (`mode=active-backup`) or do load balancing on the network connection (many `balance` modes).

linux (or maybe software that runs on linux) does have support for network bonding, which makes it much less of a pain of... not having it.

---

i had a wired connection already being used, and another way to connect to the router: wireless.

so, with some resources (like [this](https://unix.stackexchange.com/questions/272935/bonding-wired-and-wireless-while-using-network-manager/504465#504465)), i get a bond that almost works, with everything else working as expected, including unplugging the ethernet.

## the problem with the solution
the reason it almost works is because it doesn't... across reboots.

when i turn the server on, it first uses the wireless (regardless of the primary device being the wired connection) resulting in getting the wrong private ip address which isn't going to work in my case.

and so, after lots of figuring out...

## solution for the problem with the solution
the solution to the problem of using wireless at first?
create multiple bodge scripts that ups the bond connection if the ip address is not the expected one and then run it in a cron job every minute.

there wasn't really a good enough solution i could find (other than the one i just spent like the hour making).

in case you want the scripts, here are the scripts:

`check_private_ip.sh` (runs every minute, checks if the bond has the right ip address, otherwise try turning it off and on again and hope it works):
```bash
#!/bin/bash
output=$(ip addr show dev bond0)
grep "<expected private/server ip address>" -q <<< $output
[[ $? == 0 ]] || { nmcli con up bond; }
```

resetup_bond.sh (runs after every startup, turns off unwanted connections and turns on wanted connections):
```bash
#!/bin/bash
nmcli c d <wireless-notbond-connection>
nmcli c d <wired-notbond-connection>
nmcli c u bond-wlan
nmcli c u bond-eth
nmcli c u bond
```

replace everything in angle brackets with the values. then add to cron with the right timings, and hope that all this works.

---

because it was kind of a lot of work to set this up for the main server, i might not do it on the other server because while being older than the main server, it has support for wireless 5G and i have more concerns about the wired cable being possibly more unreliable.

after lots of work, it works.
and i think i'll just move on now.

# results
the result of all of that work is something so great to the point i'm just kinda surprised at some parts, like how fast the server switches from wired connection when it fails to the wireless connection.

the result also prevents the future problems of having the ethernet disconnected (more uptime/accessibility time), until the server reboots without the ethernet.
then which there's still a problem with getting the wrong ip address, and i don't know how to solve that.
but i think i've gone too far already with the bodging, so i'm stopping here.

# additional resources
- <https://www.kernel.org/doc/Documentation/networking/bonding.txt>

[bonding]: https://en.wikipedia.org/wiki/Link_aggregation
