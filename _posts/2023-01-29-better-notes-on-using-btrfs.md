---
layout: post
title: better notes on using btrfs
categories:
- btrfs
tags:
- rewritten
date: 2023-01-29 23:27 +0000
---
on 2023-01-15, i published a post named "notes on using btrfs".

it was a rant post. it was bad. and i deleted it, making that post the first
ever post that was deleted on this website after it was published.

after further attempts to use `btrfs replace`, i'm going to rectify the
previous post by making a completely new post with a better information:rant
ratio.

# what happened previously?
i wrote a rant post complaining about `btrfs replace` just stopping for no
apparent reason.

i still don't know why. i don't think it should happen, nor should it
happen to users.

the post was deleted (but actually it's still available through git
history).

# so what things have i learned?
- `btrfs replace` works even if you're missing a bad drive if you're using
  a RAID profile (if not, your data is as gone as your drive is).

  it's documented in the btrfs manual:

  > If the source device is not available anymore, or if the -r option is set, the data is built only using the RAID redundancy mechanisms.

  (that sentence seems to conflict with what the `-r` option has to say)
- `btrfs replace` seems to stop when it's early in it's process (0.0% done)
  and encounters some kind of... corruption error? no idea
- the btrfs filesystem must be mounted for `btrfs replace` to work
- mount a btrfs filesystem with the `degraded` option (give `-o degraded` to
  `mount`) if you can't have both the old and new drive available
- because you just mounted your btrfs filesystem with the `degraded` option,
  a `single` profile now exists on your btrfs filesystem. you must remember
  to `btrfs balance` after `btrfs replace` accordingly to get rid of single
  profiles if that's not the intended profile. (ref to `btrfs(5)` section
  `FILESYSTEM WITH MULTIPLE PROFILES` for further information and action)
    - considering that, it's possible you might not even have space after
      replacing a device. idk, only thought of that after i nearly ran out
      of space after replacing but forgetting to balance.
    - `btrfs balance` doesn't work with less than 4 drives when converting
      to `raid10` profile IIRC...
- `btrfs replace` seems to work after a `scrub` with the source dev
  available during the scrub
- the size of the source dev must be the greater than or equal to the
  destination dev (using 2 terabytes drives? make sure you allocate
  2000000000000 bytes and not any more, since drives can have slightly
  different sizes)
- btrfs filesystem == `{better,butter,b-tree} filesystem filesystem` ==
  btrfs fs
