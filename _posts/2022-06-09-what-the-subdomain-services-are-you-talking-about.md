---
layout: post
title: what the subdomain services are you talking about?
date: 2022-06-09T17:30:13+02:00
author: jacksonchen666
tags: [subdomain services]
---
> what the subdomain services are you talking about?

\- self

well, i'm talking about self hosted applications like [mastodon], [peertube], [synapse] and more that are under the subdomains of jacksonchen666.com or my onion ([given that it is usable]({% post_url 2022-04-15-not-onioned %})).
the subdomain services are not hosted by gitlab pages, but rather with my own servers.

it's called subdomain services because it's "services" hosted under a "sub-domain". get it?

now, i'm writing this post to clear up any possible confusion, but also to list all the subdomain services that i have.

because there's already a list somewhere else on this website and to reduce the duplicated effort of listing all of my subdomain services.
which also happens to be the [privacy policy page on this website for the subdomain services](/general-other-privacy), which you might want to read to know what you are getting into when you visit one of my subdomain services.

[mastodon]: https://joinmastodon.org/
[peertube]: https://joinpeertube.org/
[synapse]: https://matrix.org/docs/projects/server/synapse
