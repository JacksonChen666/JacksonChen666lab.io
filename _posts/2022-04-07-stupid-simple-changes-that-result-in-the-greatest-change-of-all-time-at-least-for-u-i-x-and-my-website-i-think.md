---
layout: post
title: stupid simple changes that result in the greatest change of all time (at least,
  for U{I,X} and my website. i think)
author: jacksonchen666
categories:
- website
date: 2022-04-07 20:09 +0000
---
hey, have you been on my main website <https://jacksonchen666.com> aka not [new blog]?
if so, you might've noticed that the navbar (links below the first header on all pages that depend on the `bare` layout) is a bit tight.
tight enough for it to be a bit too hard to "what which how and HOW" (what is this, which is which, how do i click, HOW DO I CLICK (mobile user special)).

if you've noticed that before 2022-04-07, then great! because i, didn't.

# how i realized
well, by changing the space between each navbar link.

what lead me to do that?

# UX survey - sample size 1
in case you're like "wtf is UX", it's User eXperience.
and in case you're wondering who i was surveying, it's some random friend i met previously IRL and now i can't cause they moved a country.

out of the blue while i was talking to someone over discord (i'm not sure of how to convince them to move when i might have to move >1 person), i <sup>mini</sup> challenged them to submit a question.

if you didn't know, [user submissions] was now over tor to avoid spammers (though i have an idea to replace it with javascript base64 decoding of the url that was generated at build time to make it only slightly harder for bots and easier to dodge bots that don't care enough)

guess what? they managed to do it and called the experience "pretty smooth" (completely disregarding that they submitted their question to a black hole the first time).

however, they did give me some feedback about the navbar: "make the separation between each link more apparent smh" i.e. make it obvious which links are different from each other

so, i thought about that for a bit. what css property do i use? ah right! `padding-right`!

## css not deepdive
`padding` adds space to within element (make links larger including where you can click it) and `margin` adds space to the outside of the elem tn (adds empty space, doesn't change the size of the clickable link).
hopefully that explanation is enough, because later i decided to just use `margin` instead.

# the realization
i added some space (`0.5rem` to be precise), holy crap does it feel so much better

![navbar with extra spacing]

just to compare, here's the one without the spacing:

![navbar without extra spacing]

can you feel the difference?

# the devil is in the details - sample size 1
yeah, it was just one person who i surveyed about the questions submissions thing to see if it was too hard.
then it spiraled into some tangent about the navbar.

i did show it to them, and they agreed that the design that i went with (`margin-right`).

[new blog]: https://blog.jacksonchen666.com
[user submissions]: /user-submissions
[navbar without extra spacing]: /assets/images/posts/navbar/spaceless.png
[navbar with extra spacing]: /assets/images/posts/navbar/spaceful.png
