---
title: trying out many things and seeing if i will even use it
author: jacksonchen666
date: 2021-11-25 22:42 +0000
categories: [experiments]
---
i've recently added more dns records because i've been trying out some stuff. here's the list of the things i've tried and my experience so far:

# [matrix server](https://chat.jacksonchen666.com)
already talked about this before so that's all

# [peertube videos](https://videos.jacksonchen666.com)
i have some videos.
i don't have a plan to upload all of my older youtube videos. but i could upload the originals.

only has 480p and 1080p to minimize time spent encoding until federated video encoding is figured out (by peertube) lol

# a calendar
on a server

that's about it

and syncing with thunderbird without third party services involved, nice

(i intended it to be a publicly viewable calendar (with little details about the events) but i wasn't able to figure things out but it's still a nice to have)

# [microblogging](https://microblogging.jacksonchen666.com)
i write stuff on there and to see if zola will be good or not.

probably not with a theme that requires more custom config and probably not without the timezone part for iso 8601

# [micro](https://micro.jacksonchen666.com)
mastodon for the purposes of microblogging.
i know it's another one of the same thing, but this it is much accessible to post on than writing and pushing changes that would only work on 1 computer.

also, there's no sign ups.
if you want to sign up for a mastodon account and toot @ me, [maybe this could be one of the choices](https://mastodon.social).
yes, there's a lot of choices out there for a server.

# stuff that i do plan on keeping ordered by likeliness
1. calendar
1. peertube videos
1. micro
1. matrix server
