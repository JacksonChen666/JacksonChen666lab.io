---
layout: post
title: gentoo handbook goes on the next level (maybe)
author: jacksonchen666
categories: [jsless]
date: 2022-03-18 10:30:46 +0100
migrated: true
---
i was just trying to install gentoo (i have done it a few times, but this time i am doing it again because why not), and i came across the 7 commands of mounting for chrooting.

it was going to be some work to copy all of them.

but for some reason, clicking on the text would result in highlighting the entire command making it easy to copy but hard to select part of the command (in fact, i can't even do a part of it, only the whole thing or not). and <u><abbr title="JavaScript">JS</abbr></u> was disabled.

it doesn't matter why JS was disabled, but things that complex (low-medium complexity) done without JS amazes me because "it must be really clever".

after scraping the surface, it's [css `user-select`](https://developer.mozilla.org/en-US/docs/Web/CSS/user-select) with the `all` value.

hope you learned something, maybe share it with your dev friends i dunno.
