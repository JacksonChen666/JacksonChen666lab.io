---
layout: post
title: nuking the related posts section and replacing it
author: jacksonchen666
categories: [website]
date: 2022-01-06 21:50:29 +0100
---
this post has nothing to do with the new blog, but instead of the old blog.

you know, it sucks to have to [wait for 2 minutes for a site to build][gitlab ci] because you've written posts that are nearing 1000 words, **twice** ([this one]({% post_url 2021-08-10-reject-great-website-embrace-great-minimalism %}) and [this two]({% post_url 2021-12-18-the-deep-underlying-workings-of-a-http-request-by-guesswork-and-some-stuff-thats-true %})) and you've also written many other posts with some and many words..

why is it an issue? i will run out of ci minutes and then be unable to publish my site then.

# the lsi
the lsi in jekyll can make related posts section.
caveats? [it's][jekyll slow and fast test] [damn][jekyll alternative for lsi] [fucking][jekyll lsi replacement] [slow][jekyll lsi experience and replacement].

here's the [gitlab ci struggling][gitlab ci] actually, just as slow as my server that's used to host the new blog and the tor site of this thing. and BY THE WAY, have i mentioned that the server is a laptop that's probably somewhat underpowered (or sluggish) for stuff?

# the solution
*yeet*

*drilling sounds*

tada, older posts or newer posts:

[jekyll slow and fast test]: https://github.com/Jashank/jekyll-lsi-test/#benchmarks
[jekyll alternative for lsi]: https://github.com/jekyll/jekyll/issues/4024
[jekyll lsi replacement]: https://footle.org/2014/11/06/speeding-up-jekylls-lsi/
[jekyll lsi experience and replacement]: https://ibug.io/blog/2020/05/jekyll-better-related-posts/
[gitlab ci]: https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/jobs/1949480316#L103
