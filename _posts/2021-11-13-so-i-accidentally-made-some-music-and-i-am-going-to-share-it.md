---
title: so i accidentally made some music and i am going to share it
author: jacksonchen666
categories:
- music
permalink: /music/random-mousic/
date: 2021-11-13 12:44 +0000
last_modified_at: 2022-04-17T21:43:08+02:00
---
yesterday, i was on garageband just looking through loops that are under the dubstep genre.
after finding some interesting loops and putting them into the thing, i kept doing it.
then it turned into something that's kinda music (and loopable).
and then i actually continued creating.

i was making music just out of the blue while waiting for garageband to download the whole sound library, which took hours to download.
and in that time, music creation happened.

# downloads
- direct: <https://files.jacksonchen666.com/music/random-mousic/>
- [the internet archive (24bit flac for lossless, or media player)](https://archive.org/details/random-mousic)
- ipfs: /ipfs/QmXitFyD9mKhr6va2mB58CFHw8b7QaKdYXE6mDv4EmyUtG

# licensing
This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by/4.0/>.

<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Sound" property="dct:title" rel="dct:type"><a xmlns:cc="http://creativecommons.org/ns#" href="https://jacksonchen666.com/music/random-mousic" property="cc:attributionName" rel="cc:attributionURL">random mousic</a></span> by <a href="/authors/jacksonchen666/">JacksonChen666</a> is licensed under a <a rel="license noopener noreferrer" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

# will you upload it to creative commons?
[not when the privacy grade is s-](https://tosdr.org/en/service/2031)
