---
migrated: true
title: the one step i completely forgot about when changing my gpg key
date: 2021-12-18 12:04 +0100
author: jacksonchen666
categories: [gpg]
last_modified_at: 2023-01-12T09:50:16+01:00
---
i changed gpg keys to use ed25519 instead of rsa4096. most stuff also changed with it except for one: ssh
i kept trying and trying, and i miserably failed. i kept trying, not looking fully on how to enable ssh for gpg keys.
until today.

today, i backed up my old gpg key (revoked and deprecated), delete it from my keychain, and updated `~/.gnupg/sshcontrol` to reflect the key to use. still doesn't work.

oh yeah, have i mention the one step that i completely forgot about? 
it's of course, updaing `~/.gnupg/sshcontrol`. i discovered it [on this article](https://opensource.com/article/19/4/gpg-subkeys-ssh)

don't forget to do that, and share it to friends who are struggling with ssh keys using gpg keys after they switch to another gpg key.

edit: i have since figured it out by [a question](https://unix.stackexchange.com/questions/643120/gpg-agent-macos-doesnt-provide-any-key-for-ssh), not the answer.

> when the question fixes your problem
> 
> <https://unix.stackexchange.com/questions/643120/gpg-agent-macos-doesnt-provide-any-key-for-ssh>
>
> (used keygrip for sshcontrol file)

\- [jacksonchen666
  2021-12-18](https://micro.jacksonchen666.com/@jacksonchen666/107468929710725109)
