---
layout: post
title: how i use tor
date: 2023-01-04 14:47 +0000
---
i'm assuming you know what the [tor project][tp] is. i tried explaining, but
life is too short for me to repeat words.

[tp]:https://www.torproject.org/

# influence from drew devault, again

after reading [drew's blog post about "bringing more tor into your
life"](https://drewdevault.com/2015/11/11/Bring-more-tor-into-your-life.html),
i started to use tor for more than just hosting some onion sites (for my
main website and etc.).

the first order of business is setting up an IRC bouncer... or something
like that.

# IRC over tor

after setting up the [software][heisenbridge], i went and setup the
connections on the server... and it didn't work. why?

[heisenbridge]:https://github.com/hifi/heisenbridge

the IRC networks i was connecting to was scanning for "open proxies" at
connection time. and i happened to be qualified for an "open proxy" (for
having port 80/443 open), so i was ~~banned~~ prevented from connecting. the
solution to that was to run the IRC connection over tor using the provided
onion service (e.g. [libera.chat][lconion]).

[lconion]:https://libera.chat/guides/connect#accessing-liberachat-via-tor

one down, any amount of things to go...

# my website (already done)

did you know i have an onion service?

yeah, you can fire up tor browser and use the onion address [in the mirrors
page]({% link mirrors.md %}) or go directly to my clearnet site.

# server admin over tor?!

did you know i have an onion service which i can use to connect to my server
with ssh?

[yeah it's possible][onionssh]

[onionssh]:https://matt.traudt.xyz/posts/2017-06-18-mosh-over-tor-except-not-really/

(note: [mosh] is mentioned in the article, which additionally runs over UDP.
tor only supports TCP, so UDP can't go over tor. mosh isn't absolutely
necessary in the setup though)

[mosh]:https://mosh.org/

i had setup that secret onion service so that in case anything goes wrong
with the ports on the router (or something with the server), i am easily
able to remote in because of tor's ability to host an onion service without
requiring open ports.

# i can't think of anymore

i can't think of anymore things i run over tor.

so that's it for this blog post. bye
