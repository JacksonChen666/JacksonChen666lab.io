---
title: why i didn't switch to "main" as the default branch name
author: jacksonchen666
date: 2022-04-04T00:22:00+02:00
migrated: true
tags: [git]
---

so git is a version control system. whenever it happened, it was decided to change the default branch name from previously master to main because of apparent concerns of referring to master and slavery.
oh yeah, [i heard you wanted to bash a cat with a pipe](https://i.redd.it/i8wiq9ddhrx71.png)[^1]? oh wait, you didn't mean it literally, but instead it was about your computer thing? oh, makes sense.

# wait but what's your reasoning for not switching?
changing a branch name isn't backwards compatible, nor is not changing it forwards compatible. compatibility is broken no matter what you do (or don't do).

when you change a branch name (especially the default branch), muscle memory and things will run into problems, including relying on a hard coded name for what is assumed to be the default branch, but the name of the branch doesn't exist so it breaks.

# did you have any problems when changing the default branch to main?
yeah i think so (though), it's called "wait that's not the branch name" problem where i `git checkout master` and it basically says "what"
and so i check `git branch` to realize it's supposed to be `git checkout main` and not `git checkout master`.
it happened to be because [this project](https://github.com/Frontesque/VueTube) that i was working on (and made no significant contribution, yet) had the default branch name of `main`, which is similar to `master` but not off by the first 2 letters, which means you might mistakenly take `main` as `master`

# "it's a one time fix that make a lasting change"
well, it depends on what you mean by "one time fix". if by one time you mean you just need to execute 1 command for each repo, then good luck trying to put that definition to use when dealing with hard coded branch names because they'll probably break.

# "well, are you going to do it?"
i'm not bothering, that's all. also muscle memory.

[^1]: https://old.reddit.com/r/ProgrammerHumor/comments/qn8zu5/how_to_google_too/
