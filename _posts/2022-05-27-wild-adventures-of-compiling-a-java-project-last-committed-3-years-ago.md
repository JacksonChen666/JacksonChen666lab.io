---
layout: post
title: wild adventures of compiling a java project last committed 3 years ago
author: jacksonchen666
categories:
- wild adventures
tags:
- java
- compile
date: 2022-05-27 19:41 +0000
---
i was doing a lot of compiling software to install it by using gentoo in a vm.
it's been the thing i turn to when i'm kinda bored and would rather watch (meaningless to me) output of a package being compiled and merged into a gentoo system.
in fact, it's to the point where i wanted to compile from a launcher to a mod and then run minecraft with the mod (which didn't succeed (at the launcher part), so i didn't do it).

but at least i have macOS, and minecraft works fine on that.
maybe compile the mod on that instead of gentoo?

# the mod
what is the mod?
[this is the mod][mod page] i'm talking about.

it's only slightly alive, with the downloads increasing very slowly over the last times i checked the page.

oh yeah, fun fact: the source button wasn't there in the first place.

# asking for the source
it was only added 5 years later after the creation date, after i emailed the author of the mod about the missing source link and my inability to find the mod source code/repository.

why wasn't i able to find the repository on my own?
because i was [here](https://github.com/McJty) and not [here](https://github.com/McJtyMods) i.e. i was in the wrong place and the author knew the right place.

# compiling the source code
now that i have where the source code is, i `git clone`'d [it][mod source] and also [a dependency][dep 1 source].

i first try to compile the dependency, but it doesn't work, resulting in an error of being unable to send a GET request to <https://tehnut.info>, which at the time of writing is still a dead server (for some reason).

i try to find more information, but ultimately i just commented out [the maven repo](https://github.com/McJtyMods/McJtyLib/blob/446bd059c970a713e52a159f29b39cf8c7572f4a/build.gradle#L27-L31) that was causing problems.

then, i got an issue with [a specific dependency][dep 2] that just... no longer existed in the set repositories.

## dealing with missing libraries
now there's something missing, i tried to compile [the specific dependency][dep 2], and it's source was [here][dep 2 source].

i `git clone` it, run `./gradlew build` it works.

i go back to the [main mod dependency][dep 1 source], doesn't work, same error message.
i then get an idea: what if i do a local publish, copy resulting locally published files to a web server, and then add a maven repo that contains the needed dependency?

i did do that.
there was a minor problem with [this mod][dep 2 source], and it was the versioning being 1.0.0 instead of 1.0.63. so i had to rename it before it would work.

---

after that was dealt with, i compile [this mod][dep 1 source] and then it works.
now that the issue is fixed, i apply the same thing to the [main thing i'm compiling][mod source].

and voila, it works and i have a jar file.

i guess i'll go play minecraft now idk.

[mod page]: https://www.curseforge.com/minecraft/mc-mods/need-to-breathe
[mod source]: https://github.com/McJtyMods/needtobreath
[dep 1 source]: https://github.com/McJtyMods/McJtyLib/tree/1.12
[dep 2]: https://github.com/McJtyMods/McJtyLib/blob/446bd059c970a713e52a159f29b39cf8c7572f4a/build.gradle#L79
[dep 2 source]: https://github.com/Darkhax-Minecraft/Tesla/tree/1.12.2
