---
title: I have a matrix server now, I guess
author: jacksonchen666
categories:
- matrix
date: 2021-10-08 15:57:06 +0200
---
Whelp, now I moved my blog posts onto <https://www.jacksonchen666.com> and now that's the blog still hosted by GitLab as usual.

# Why did you have to move your blog posts onto the www subdomain?
Delegation hell (the element client just kept requesting at my main domain instead of the matrix subdomain) and because jacksonchen666.com as a homeserver name is better than matrix.jacksonchen666.com

As of now, if you try to use my domain (without the www subdomain) as a homeserver (for matrix), you can't sign up.
That's because I'm not yet ready for complete operation and management of the homeserver (including writing the terms of service, privacy policy, and code of conduct).

# A matrix server? Why?
Yep, decentralized and secure chat cause other options just aren't great in a privacy sense (I'm kinda hardcore privacy guy. Definitely not extreme hardcore).

# Can I join?
Sure! Just make sure you read and understand the terms of service and the privacy policy beforehand (I'm being all serious tho). I'll try my best to keep it as short as possible.
And that you wait for me to actually finish those uncompleted documents.

Now, onto my favorite part that I've already been doing:

# Fully Anticipated Questions
### Where can I chat?
If you're a newbie to <https://matrix.org> (which from the question alone, you likely are), you can chat at <https://chat.jacksonchen666.com> (or [bring your own client](https://matrix.org/clients/)).
If you can't sign up, I'm still not ready.
Check back next time.

### Why can't I change homeservers on chat.jacksonchen666.com?
You're gonna chat on jacksonchen666.com, as I guess the name implies.
If you want to switch homeservers, [this is the exact client I provide on chat.jacksonchen666.com](https://app.element.io)

### Federation?
Should be working, so you can join rooms in my homeserver with other home servers I guess.

### Can I join rooms on your homeserver, but without signing up on your homeserver?
Probably depends on federation.
Oh, and you'll need another homeserver to even chat on the matrix network.
You can setup your own however.

### Email sign up and login?
Soon(tm)

### Phone number sign up and login?
Probably not (but it really depends on if I have to send SMS messages, which I very likely will not allow)

### What's considered personal information?
Email addresses and phone numbers, physical addresses etc. It's best to not share them with anyone really.

### Password resets?
Via email.

### Will I have to provide my personal information?
Providing personal information is at your choice (so you can choose to not provide ANY personal information at all).
If you do provide personal information, some stuff can become possible.
If you don't, then some features are just plain not available for you.
And it's not like I like harvesting personal data, it's just that there's no way to prove you're you unless we use some manual methods of verification.

### How can I prove that I am me if I lose my password and didn't provide my email?
Well, it can become quite complicated really quickly.

Suppose you [message me (@jacksonchen666:jacksonchen666.com)](https://matrix.to/#/@jacksonchen666:jacksonchen666.com) right after you sign up with your OpenPGP public key (with or without email address attached to the key).
I will then associate your matrix account with the OpenPGP public key.
In the event of losing your password, you can sign your message (maybe even encrypt it for me) to send an email (from any email address) request for a password reset.
I will reply back with a reset password link instead of a password in an email, encrypted with your public key and signed with my private key (so you can be sure it's from me).

The email address for password reset requests is still to be determined, including an automated system.

However, if you didn't message me with your OpenPGP public key right after you signed up and you get locked out, then you're officially locked out of your account, and I'm afraid there's no other way you can reset your password (unless I've met you in real life and exchanged contacts that can be trusted to be the real you, and use the alternative communication channels for recovering your account)

### What about Tor users?
I respect Tor users. However, I have no idea what to even do to set a matrix server up to include Tor users, so I'll have to do some research beforehand.

### Privacy policy? I hope it won't be too long
Hopefully. Just have to use less words while not throwing out critical details, and use simple language.
