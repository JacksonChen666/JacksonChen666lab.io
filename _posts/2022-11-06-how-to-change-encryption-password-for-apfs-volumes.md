---
layout: post
title: how to change the encryption password for an APFS volume with diskutil
author: jacksonchen666
tags:
- APFS
- encryption
date: 2022-11-06T23:17Z
---
i have some external drives that are formatted with encrypted APFS.
i needed to change the password for the drives, but there was of course, no immediately obvious way to do so in Disk Utility.app.
so of course, the first course of action is to make a draft blog post about how to do so, so that it could help someone else out when they need to change the password but can't find the option through Disk Utility.app.

# Disk Utility.app is broken and it's not your fault
[the asahi linux project recommends against using Disk Utility.app](https://nitter.namazso.eu/AsahiLinux/status/1585475289974333442) (note: links to a "random" nitter instance) because it can barely handle real-world partitioning, and [this section pretty much goes into further detail on why](https://github.com/AsahiLinux/docs/wiki/Partitioning-cheatsheet#do-not-use-the-disk-utility-application).

to basically sum it up, Disk Utility.app is user-friendliness over actual functionality. `diskutil` is functional, just not as user-friendly.

(note that i tried to find in the help thing on the toolbar, but this post is no longer about Disk Utility.app)

# DISCLAIMER OF LIABILITY
The following is [Section 5 of the Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/legalcode#s5) verbatim:
> Section 5 – Disclaimer of Warranties and Limitation of Liability.
> 
>    **a. Unless otherwise separately undertaken by the Licensor, to the extent possible, the Licensor offers the Licensed Material as-is and as-available, and makes no representations or warranties of any kind concerning the Licensed Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether or not known or discoverable. Where disclaimers of warranties are not allowed in full or in part, this disclaimer may not apply to You.**
>
>    **b. To the extent possible, in no event will the Licensor be liable to You on any legal theory (including, without limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive, exemplary, or other losses, costs, expenses, or damages arising out of this Public License or use of the Licensed Material, even if the Licensor has been advised of the possibility of such losses, costs, expenses, or damages. Where a limitation of liability is not allowed in full or in part, this limitation may not apply to You.**
> 
>    c. The disclaimer of warranties and limitation of liability provided above shall be interpreted in a manner that, to the extent possible, most closely approximates an absolute disclaimer and waiver of all liability.
> 

Basically, if you fuck this up, you fucked it up and **I will not be liable for your mistakes and losses.**

Avoid copy pasting blindly, because that could lead to fucking up.

with that out of the way, let's start taking some pre-cautions.

# pre-cautions
the main thing here is to limit damage in the case that you make a mistake and lose things.

1. make a backup of everything

   if you're going to mess with partitions, do a backup of the partition you're going to change right now (and all drives that are connected but cannot be disconnected like the internal drive).

   you may want to use time machine on macOS as a backup solution, which is built in and works pretty well (in my case).
2. unplug all unrelated drives

   you should also unplug all unrelated drives from your system.
   **ejecting** ***is not*** **sufficient, they will remain unmounted but still available for manipulation and destruction**.
   physically disconnect unrelated drives from the system.

3. don't do this on a macOS install (unless you know and understand the consequences)
   
   if you want to change the password for a macOS install, change the password for the user *in* the macOS install.

this way, if you make a mistake and then somehow destroy all data on all connected drives, you would have 1. a backup and 2. not lost an unrelated drive's data (restoring takes *time*)

# how to change encryption password for an APFS volume with diskutil
(note: this guide is based on [an answer on apple stackexchange](https://apple.stackexchange.com/questions/309541/cannot-change-apfs-encryption-password) and was tested on macOS monterey 12.6.1)

1. open Terminal.app (or your preferred terminal app if you have one)
2. run `diskutil list` to list the drives

   example output (trimmed parts that are not relevant):
   ```
   [...]
   /dev/disk7 (external, physical):
      #:                       TYPE NAME                    SIZE       IDENTIFIER
      0:      GUID_partition_scheme                        *2.0 TB     disk7
      1:                        EFI EFI                     209.7 MB   disk7s1
      2:                 Apple_APFS Container disk8         2.0 TB     disk7s2
   
   /dev/disk8 (synthesized):
      #:                       TYPE NAME                    SIZE       IDENTIFIER
      0:      APFS Container Scheme -                      +2.0 TB     disk8
                                    Physical Store disk7s2
      1:                APFS Volume BACKUP                  1.5 TB     disk8s1
   ```

3. find your drive

   find the drive with the same, and check the type is `APFS Volume`, then note down the identifier.

   note: the size is the used space size.

   in my case, the volume name is "BACKUP" which has an identifier of `disk8s1`, which is the volume i'm looking to change the password for.

4. run `diskutil apfs listUsers <drive identifier>` (replace `<drive identifier>` with the drive identifier, which is `disk8s1` in my case)

   example output:
   ```
   Cryptographic user for disk8s1 (1 found)
   |
   +-- C9101128-C9E2-47DB-A011-F99D8B0EE7CF
       Type: Disk User
       Hint: asdf
   ```

   check that the `Hint: ` is the password hint you have set (if you remember it).

   note down the random looking string (technical: UUID) above the line `Type: Disk User`.

5. run `diskutil apfs changePassphrase <drive identifier> -user disk` while replacing `<drive identifier>` with the actual drive identifier.

   it will then prompt you for the old password with the following prompt:
   ```
   Old passphrase for user C9101128-C9E2-47DB-A011-F99D8B0EE7CF:
   ```

   the random looking string (or UUID) should match the one you found in step 4.
   if it does not match, check that you got the correct drive.
   if you want to exit the prompt, press the Control key (*not the command key*) and the letter C to interrupt the program.

6. enter your old password for the drive and enter your new password.

   this is the main part: you enter your old password, then enter the new password twice (to confirm you typed it correctly), then it's complete.

7. (conditional) if you enabled the option "Remember this password in my keychain", the password that was saved in your keychain is no longer correct.
   to fix that issue, eject, unplug, and replug the drive you just changed the password for.
   then re-enter the new password when the prompt comes up and also select "Remember this password in my keychain."

and that's about it for one volume.

this password change does not apply to the entire drive, just the volume you applied it to.
so you will have to repeat this for all the volumes.

sidenote: if you want to change the hint, do step 5 but change `changePassphrase` in the command to `setPassphraseHint`, and add to the end of the command `-hint "<hint>"` replacing `<hint>` with the hint (keep the `"` quotes in the command). an example is `diskutil apfs setPassphraseHint disk8s1 -user disk -hint "zxcv"`. the output will be different and it will not prompt you for anything.
