---
layout: post
title: wait, why is port 53 open on my server?
categories: [networking]
author: jacksonchen666
date: 2022-03-15 14:44:12 +0100
migrated: true
---
while going through [hackernews](https://news.ycombinator.com/news), i came across [this post](https://news.ycombinator.com/item?id=30661852) linking to [this page](https://bastian.rieck.me/blog/posts/2022/server/).
and i thought to myself "hey, maybe i should do a bit of security check up i guess"

so i ran a port scan with `nmap` on my server (public ip address).
i see 3 ports i didn't open myself:

- port 21 - ftp
- port 23 - telnet
- port 53 - wait that's kinda suspicious

# port 53: wtf is it?
it's [DNS](https://en.wikipedia.org/wiki/Domain_Name_System). DNS servers would have this open, but for some reason, it is open on my server, which is not a DNS server. it's a HTTP server along with many trash.

# looking for the cause
i check on my main computer, no port 53. main server (all web services except main website), no port 53 except locally, not much of an issue there. other server (very little public facing), wait netstat command not found. search on apt, found nothing. internet search, install `net-tools`, works, and not even 53 **anywhere**.

i try `dig` (dns query program) and make it query my server. it works. everything about dns works. that's not good.

i have an idea: port scan the entire local network. i almost go for `nmap` then stop myself to actually use `zmap` instead.

[`zmap`][zmap] is [`nmap`][nmap] but for scanning the **entire internet** for a specified port. well, there shouldn't be a lot of ip addresses to scan for, so i go and do it.

after it finished, the results are:

- the router

that's all the online devices that have port 53 open.

and that makes a bit sense because the router sets the default DNS servers (for any clients) to the router itself. except that it is now public to the internet.

i scan for port 21 and 23, not a single device has the port open.

i try to restart the router, no changes.
i try using upnp to check if it's enabled. i used [miniupnp](https://miniupnp.tuxfamily.org/) and tried to open a port. doesn't seem to be enabled anyways, so probably no problems there.

i try an online port scanner and it... didn't work.
for a sanity check, i checked for port 22, which was opened intentionally. they said it was open.
oh... uhhhh... what?

# why online port scanners might've not worked
online port scanners is just another server literally doing port scanning at your request.

i'd assume because the router routes requests that are from and to itself, it actually never sends the request out to the internet from and to itself (might be because it's not possible)

# but nothing makes sense anymore
i connect to my phone for cellular, then try nmap again. it says it's open. i really turned off the vpn on my phone, which would connect back to the router and then do the thing all over again.
**WHERE CAN I FIND THE CAUSE OF THE ISSUE???????**

"oh wait i still have the vpn activated on my computer oops"

yes, i have a vpn that's for myself and no one else. i only use it to circumvent any local network blocks it might have when i go somewhere else.
i deactivated that vpn and now the port is not really open, it's filtered just like 21 and 23.
and then i try the default 1000 ports that's usually common and i got heavily throttled to the point of taking 2 minutes to do all of the 1000 ports.

# conclusion
it was never open in the first place, because i was basically scanning the router internally instead of externally.

check that you actually have it open to the internet by not being connected to the exact network you're trying to scan outside, because the router might scan itself internally and you might go down a spiral of "nothing makes sense".

[nmap]: https://nmap.org/
[zmap]: https://zmap.io/
