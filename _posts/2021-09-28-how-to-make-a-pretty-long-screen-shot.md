---
title: How to Make a Pretty Long Screen Shot
author: jacksonchen666
categories:
- tutorials
tags: [ios, shortcuts]
date: 2021-09-28 14:43 +0000
---
Ever wanted to make a long chained screenshot but you don't have the feature of something like that?
Well you can do that manually as well.
If you want to create a long screenshot/image manually (from multiple screenshots/images), then read on.

It is assumed that you have screenshots of the same size, and full screen (to avoid misalignment).
If you don't have either or both of them, realignment of the images may be required.
If you want to remove some parts like some of the left side for all of the images, do that *after* chaining the screenshots together.

# Method 1: Using image manipulation tools (e.g. GIMP, Photoshop)
Steps:
1. Create new image (any size, generally very large depending if you want it horizontal or vertical)
2. Import an image
3. Crop the images to the contents of the screenshot/image[^1] [^2]
4. Repeat step 2 thru 3 until you imported and cropped all the images
5. Crop off the parts where you don't want (including parts that doesn't go well with the flow)

## Pros and Cons
Pros:
- You have control over the images (including doing bad cropping)

Cons:
- It's manual labor chaining the screenshots together
- You must crop each image before you can chain them together

# Method 2: Using Apple Shortcuts
1. Crop all the screenshots so it's continuous
2. [Install this shortcut](https://www.icloud.com/shortcuts/1d86b2f7f6a9455c80a4aa47b9293559)
	1. You must enable "Allow untrusted shortcuts" in the settings app, going to the shortcuts settings in the settings app[^3]
	2. Add the shortcut (review that the [shortcut matches](/assets/images/posts/longss/shortcut_long.png))
	3. Run the shortcut
		1. Select images you want to use
		2. Select how the images should be chained
		3. Wait for it to combine the images
		4. When it shows the long image, you might want to inspect it. If you want to save it, press the share button, and press "save image".
		5. Close the quick view prompt

## Pros and Cons
Pros:
- Less manual labor

Cons:
- Likely less precise (you kinda don't have pixel precision)
- Alignment isn't exactly possible when trying to do it seamlessly

# Potential Caveats
- Image compression will dawn on you if the service you upload to compresses harder based on size of image
- Non-seamless long screenshots are possible, so avoid gradient colors on things like chat messages if you do care about seamlessness

[^1]: To avoid the [r/croppingishard](https://www.reddit.com/r/croppingishard) curse, don't crop horizontally when making a vertical long screenshot and don't crop vertically when making horizontal screenshots. Only crop them in any way after chaining together all the screenshots.
[^2]: If you want to, you can go and zoom in the picture to the pixel level where 4 pixels could be on your screen max at the same time, and then crop in pixel level precision.
[^3]: iOS 15 may not have that option obviously out there. I'm not sure where that went.
