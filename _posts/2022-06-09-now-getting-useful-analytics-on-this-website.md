---
layout: post
title: now getting useful analytics on this website
date: 2022-06-09T22:25:42+02:00
author: jacksonchen666
categories: [plausible]
tags: [analytics]
last_modified_at: 2022-06-09T22:45:55+02:00
---
so, i decided to try out [plausible](https://plausible.io/) self hosted (as i do with most things).
the thing is, not knowing your audience isn't really great exactly, including viewers.
so i decided to commit to experimenting with having plausible on my main site.

# goals
to know more about the audience overall interests and some usage, on this website and jacksonchen666.com only.

if things do go over tor and then it does track users of tor, that's because i'm not done yet, and i will aim to not include that.

actually, you know what? i'll just use multiple configuration files for tor and ipfs and main site to make things easy to reproduce (given you know how to properly do it).

# disadvantages?
no, not really.

it doesn't log ip addresses (it does use them but not store them in plain text)

# any more?
~~no i'm in a rush~~

yes:
- [publicly available data](https://tracking.jacksonchen666.com:8448/jacksonchen666.com).
- inspired by [seth](https://sethforprivacy.com/posts/privacy-preserving-stats/)

