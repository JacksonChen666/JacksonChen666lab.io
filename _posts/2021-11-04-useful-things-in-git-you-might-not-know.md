---
title: useful things in git you might not know
author: jacksonchen666
categories:
- git
tags:
- you might not know
date: 2021-11-04 07:35 +0000
last_modified_at: 2021-11-04 11:07:32 +0100
---
on one random day i've decided to review the available options for configuration in git.
while reviewing, i've found some more stuff that could help some people working in git.
i hope this can be useful to you, and that's about it.

# the list of things
- [`git add --interactive` mode](https://git-scm.com/docs/git-add#_interactive_mode)
    - it's what it sounds like. `git add`, but interactive.
- [huge amounts of file updates made faster with `checkout.workers`](https://git-scm.com/docs/git-add#_interactive_mode)
    - this has bottlenecks, that is, the filesystem.
    - [has adjustable minimum amount of files needed to trigger parallel checkout workers](https://git-scm.com/docs/git-config#Documentation/git-config.txt-checkoutthresholdForParallelism)
- [`commit.verbose`](https://git-scm.com/docs/git-config#Documentation/git-config.txt-commitverbose), includes the diff that you're about to commit when editing the commit message with a text editor (that's all i know)
    - same as passing `--verbose` to `git commit`
- [`commit.template` specifies a template commit message](https://git-scm.com/docs/git-config#Documentation/git-config.txt-committemplate)
    - could be useful if you follow a format for all commit messages
- [`core.fsmonitor`](https://git-scm.com/docs/git-config#Documentation/git-config.txt-corefsmonitor) which should be set to a command for a file system monitor like watchman
    - this would be used to improve the performance of `git status`, as that command would not have to re-compare all files to check what changed.
    - should be useful for large changes in large repos
