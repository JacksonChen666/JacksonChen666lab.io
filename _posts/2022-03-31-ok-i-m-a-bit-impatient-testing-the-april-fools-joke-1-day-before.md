---
layout: post
title: ok, i'm a bit impatient - testing the april fools joke 1 day before
author: jacksonchen666
migrated: true
categories:
- website
tags:
- testing
date: 2022-03-31 08:31:31 +0200
---
i went [back to here](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipeline_schedules) and saw that i can start a pipeline anyways, so i started to simulate the thing.

[here is the pipeline that's for the start of april fools, but simulated](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipelines/505569324).
i then waited. then it deployed and the page didn't even update but only after reloading.

so, i reloaded the page, again and again and again and again and again and again and again this time with command+shift+r to reload without cache.

yep, it is upside down.

but because it's not april fools yet, [i ran another pipeline to revert the changes to also test that too](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/pipelines/505570956). that works. all works.
i have also changed the pipeline time to utc instead because utc.
