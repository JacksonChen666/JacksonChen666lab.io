---
layout: post
title: 'the github rabbit hole: searching for EVERYTHING'
date: 2022-09-13T09:37:48+02:00
author: jacksonchen666
categories: [github]
tags: [random]
---
today i decided to search on github for... everything

it started with some messages in the organic maps matrix room, with someone suggesting the use of a stale issue closer bot to close stale issues.
some replied that it was basically a bad idea for everything except only the issues count (this is more of my words than theirs but the main point stands).

because of that discussion, i remembered there was an issue in some jekyll plugin repo which was being bumped for... way too long.
i wanted to find that issue, so i tried searching for that issue. and it took quite a lot of tries to no avail.

as the nature of me, i got distracted into finding EVERY ISSUE IN EXISTENCE on github.

# searching for all the issues on github

someone's first instincts might try using `*`, but that's not realistic and also didn't work, yielding a billion code results (which was also a lie because it's 500M+).

github also has a thing called advanced search, which i used to my advantage to search for all issues.

turns out, it was kinda easy. the query was: `comments:>-1`, which yields 324472098 issues. then 324472091. then 324472169 (around 2022-09-13T07:53Z).
why? because things are happening, like creating and deleting issues.

the query is to find all issues with or without comments, which *should* include everything.
another advantage is that the query does not timeout (results in less results if it happens), so results *should* be complete.

also, it's not just issues, it's also pull requests.

# search for all the repositories in github
this was more complicated, because the repository searches continued to timeout.

with more tries, i have this query that resulted in 42 million repositories: `pushed:>1970-01-01 fork:true`

this is probably the best i'm able to do for now, because there doesn't seem to be other options for repositories.

# other things
sorting by the most commented throws [this issue](https://github.com/nextcloud/browser_warning/pull/50) having over 6000+ comments with a bot just repeatedly approving and commenting "Approved 👍".

## linus torvalds uses github
why am i mentioning it? because i made a query (`repos:>0`) for users which for some reason yielded linus torvalds as the first result.
i also recently saw [this](https://github.com/subsurface/subsurface/pull/3521) in his recent activities so linus does use github for something (but not for the linux kernel).

# conclusion
well, this was a kind of ridiculous adventure, but i just went through my emails and my answer to the original question i actually had before i got distracted is [this issue](https://github.com/jekyll/jekyll-seo-tag/issues/396) which was bumped for about 2 years.
