---
title: a self-served microblogging site and why i made it
author: jacksonchen666
categories:
- experiments
- website
date: 2021-11-08 11:56 +0000
---
if you have been following my gitlab, you may have noticed i recently started [microblogging.jacksonchen666.com](https://gitlab.com/jacksonchen666/microblogging.jacksonchen666.gitlab.io) on my gitlab repos (available on <https://microblogging.jacksonchen666.com>).
why did i make it? to test and learn [zola, a static site generator written in rust](https://www.getzola.org/) making site generation stupid fast (and stupid slow at monitoring file changes and reacting).
also, to have more freedom than twitter. formatting specifically. like **bold**. and *tilted **bold*** letters.

# the potential future of the main site
i could migrate the microblogging to my main site (or i could duplicate) but whatever.
microblogging random stuff was kinda done to learn more about zola, the things, and how i could use the damn functionality of search.

maybe i'll start using zola on my main site if it works well for me.

# the timeline of my website
i started my website on 2020-02-26 using crappy design skills to make my website (ew grey background)

i started using the git version control on 2020-03-05

[i went from html to jekyll on 2021-03-25](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/commit/f0217478ff3d8ec58f7667df196158026be784c6)

[and i used a theme on 2021-08-09](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/commit/acc2152c24f824855282d0772945acd811fbc0e6)
and [i ditched the other parts of my website that was garbage on 2021-08-10](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/commit/e3ffa8ab1e0f43ceedb1d8e6eb7962a447dfbea0)

and sooner or later, i'll switch to zola static site generator instead of jekyll.
sometimes, my website goes through many changes, and even faster than any other websites care.
my website is kinda just a random place where i put stuff on and that's about it.
kinda like <https://billwurtz.com> which actually started this website in the first place (his website hasn't really changed since then).

# some minor roadblocks
zola is written rust, which is a compiled language, which means a binary is the program.

the binary would have to be installed every time, so instead i went with the "custom docker images" approach again for a binary.

# making the php file
crap, i didn't think of that. idk tbh, i'll probably figure things out.

# other <abbr title="static site generator">ssg</abbr>s
[cobalt](https://cobalt-org.github.io/) is too minimal (or too hard) to use.
i was not able to get anything working.

jekyll. it works, i am ok with it. there probably isn't a reason i should switch from it.

# rss for microblogging
will exist, usually `atom.xml`

# theme changes
potentially, to [after-dark-modified](https://gitlab.com/jacksonchen666/after-dark-modified), which is the after dark theme without the intro animation.
it looks pretty nice and has some auto generations.

# questions, ideas, and feedback
i might ditch those pages if it becomes too hard to convert.
but i might also ditch it because it is unused.
anything is possible and not guaranteed.

# why tho?
why not tho?

also, ipfs doens't work well with the structure jekyll has.
and zola works well in the sense of "build and deploy with any webserver without extra configuration" because it uses index files.
