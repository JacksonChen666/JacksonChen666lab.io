---
title: why i made the submissions defunct to the clearnet
author: jacksonchen666
categories:
- website
- submissions
date: 2021-11-17 16:42 +0000
last_modified_at: 2022-03-20 01:47:06 +0100
---
1. the webhosting provider intentionally crippling the speed of the ftp server
not even the speed, the time it takes to even list things times out.
that's how much crippled it is.

    maybe that's just how free hosting works.
    crippled speeds because everyone is trying to access, or throttled speeds by the provider.

2. it is not controlled by the first party (i.e. me)

    it is not controlled by me.
    therefore, you have to put trust into the service that i'm using.
    and me included, i'm pretty sure.


the following will be reasons that don't go with my intention:

1. to stay hidden

    my goal here is to still provide a way to submit, just using another server.
    oh, and i can't exactly intergrate with gitlab pages to seamlessly blend in with the main domain.
    that would require a full website hosted by 1 server (or multiple).

and the following is more of unintended effects:

1. spammers **begone**

    unless they aren't just dumb web scrapers without human intervention

2. left out users
    
    ok but i also have no idea what you are doing on my site.
    like did you go to this page?
    what did you click?

    what i'm trying to say is i don't have a tracker.
    while i did, it wasn't useful since kinda no one is on my site.

# when will this be rolled out?
already.

# how can i submit then?
use the tor browser, open my website, upgrade to onion site (if you're not already on it), and ignore the red warning on my page (clarification: contains a text box and text in red with link to this page) because hidden services isn't the clearnet.
then submit.
