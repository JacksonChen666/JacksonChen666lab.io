---
title: Reject Great Design, Embrace Great Minimalism
author: jacksonchen666
date: 2021-08-10 11:18 +0200
categories: [website]
---
Welcome back, today I introduce you to my website with a new theme that I found on the internet, and it's called "Hacker".

OK, I didn't actually pick the theme because it sounds cool, this is basically only the theme that I found that works and not with [more stuff](https://pages-themes.github.io/modernist/)

# The reason for the switch
I transformed this website into a minimalistic (is that a word?) blog because well, most of the content on this website is not something a lot of people look at frequently.
Maybe because it's too short or not content from me.
So I deleted most of the pages from my website, played around, and then found and settled on the new theme.

Now that I think about it, I'm kind of obligated to make more posts on my website, cause that's kind of what my website is mostly now. Blog. Maybe about anything in my life or related to technology. Actually, I have 3 pages for user submission, and you actually can subscribe to [RSS](/rss) for those stuff, or just enjoy the empty flow of content on any other pages.

Now because I no longer have a sidebar (and a lot of pages), [issue #8](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/issues/8), can finally, be thrown out of the window.

# Website design, old vs new
Looking at my website more, it no longer seems to have this design contrast where the content is literally plain html, and the sidebar was the only thing that made it look better.
It's kind of odd that the horizontal lines are somewhat scuffed compared to the [preview version of this website's theme](https://pages-themes.github.io/hacker/), because it actually doesn't stretch outside the content, and I still can't figure out why is that (but I actually may keep it that way).

## Light theme
Maybe I'll consider doing that in the future, since there are legitimate reasons on why someone would use light mode, like being able to actually read the text in bright sunlight (I have had a lot of that more recently when going outside).
However, I don't have the colors chosen

## Performance
My website isn't going to fun to scroll through at the low-end, because the theme I chose actually has some text shadowing, and there could be a lot of content on this website.
And when there's too much content, and too much inefficiencies in the renderer, yeah no that's not going to be good user experience.

## Compatibility
Hopefully the compatibility will include a Kindle and an Apple Watch, since browsing on those devices are possible (but not mainly intended, usually).

While the [motherf-ing website](https://motherfuckingwebsite.com) still could beat me on literally anything else that I did not yet take care of (including IE6 compatibility, why?), I would be happy it would be more usable on more devices that has no real practical use for web browsing, and others too.

## Size
Because I don't have a sidebar with the CSS and constant repetition required to make it work, I shrinked from like around 16kb (home page, before) to 9.6kb (this page, now), which is definitely a reduction in the size.

While I could try and get a plugin to compress the HTML down further, I'm going to have to leave soon (tm) because vacation. Also, I haven't bother finding one that works with Jekyll, and I'd like to compress the HTML really down.

## Responsive design
It's not *the* great and broken for reasons I cannot understand. Maybe I should actually just solve the \<hr\> problem.

# Theme modifications
Yes, I copied the theme and changed it. The unordered list arrow was a 24x10 PNG file and looked worse than a 24x10 manually designed in GIMP by me SVG file, and there was stuff like how the image just refused to follow the guidelines of staying within the content.

# Timeline of my website
I switch to using Jekyll about 4 weeks ago apparently, and now I'm on a new design. That does not make sense to me, because my sense of time is still lost.
When I started this website like more than a year ago (like on 2020 Feburary 23rd, check memes page lol), it had the good old gray and black design with plain HTML and "I have no idea what I'm doing" design, with no version control initially.
Later I changed those colors to a "good for accessibility contrast" colors, and it was green and white I'm pretty sure (accuracy history on commit history, check that lol) and also animated the content to swipe in. It may have looked nice, but wasn't great user experience (imaging waiting a solid second for the content to show up).
Then I switched again, to the black background with plain HTML content but actually nice looking sidebar I took from [Fireship on YouTube](https://youtu.be/biOMz4puGt8).

And now, this. There's not much to say, other than "at least I don't have something that might break in the future, but now, rather the far future".

# Sneaky peaky on the tweety
Yes, there were sneak peaks on my [twitter account](https://twitter.com/jacksonchen666/status/1424650018691207171) (and more sneak peaks are not guaranteed).
While the first change I made still kinda looks nice, it's still my old website's design.	

# Fully Anticipated Questions (as Tom Scott once said)
## What stuff would you put on your blog?
Anything like rambles (maybe this), software, Linux (oooooooooooooooo)

## Is it a blog, or is it posts?
I think you can classify my website as a place for my blog and users to submit stuff to me. End.

However, Jekyll would probably still call these blogs "posts"

# End
Thanks for coming to read my random rambling about websites and stuff, lol.
