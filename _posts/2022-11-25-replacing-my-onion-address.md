---
layout: post
title: replacing my onion address
author: jacksonchen666
categories:
- website
- onion
date: 2022-11-25T17:22:04+01:00
last_modified_at: 2023-01-16T20:57:30+01:00
---
remember when i wrote about [vanity onion addresses?]({% link _posts/2021-08-19-vanity-onion-address-shenanigans.md %})
well, i found [the "onion" v3 spec](https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt?id=5642732fc037aa59199e1250afac1b96b4ed8a28#n2361) which basically says that vanity onion are silly and could train your users to do bad habits.

guess what my onion address is?
a vanity onion address; `jc666y666qdszep3qxuczj3p4saebwq4xtnl7igdoykuxhoa6taeheid.onion`.

so today, i felt like replacing it, for the sake of un-training potential users bad habits.

# stolen advice: use bookmarks
i stole the advice from [here](https://matt.traudt.xyz/posts/2017-12-02-dont-https-your-onions/#tie-the-website-to-a-trusted-name), but you should use bookmarks for onions.

it would be more difficult for an attacker, since they would either have to change your bookmark somehow or get hold of the private keys of the onion service, both of which is difficult when compared to brute-forcing a somewhat similar onion address.

# the new onion address
`x7ikq7gwf6vnbvrc7b36nkcxnw7eckwaricmjbdvrajoeql2ccjb5aad.onion` is the new onion address for `jacksonchen666.com` and everything under that.
`jc666y666qdszep3qxuczj3p4saebwq4xtnl7igdoykuxhoa6taeheid.onion` will now throw a permanent redirect to `x7ikq7gwf6vnbvrc7b36nkcxnw7eckwaricmjbdvrajoeql2ccjb5aad.onion`.
you can see the [mirrors]({% link mirrors.md %}) page for links to mirrors of my website.

the old onion address will be dropped entirely when decide to do so, meaning the old onion address should be replaced with the new one in all places or else the link will be dead.

# if i missed anything
if i still link to the old onion address anywhere, let me know via [email]({% link contact.md %}) using links to the place with the old onion address.

# the process
the process was not very exciting nor very interesting
1. create a new onion address with some options in `torrc`
2. repeat step 1 until i get a not horrible onion address (done once)
3. search and replace the old onion address in nginx
4. create a permanent redirect
5. write this post
6. reload nginx and tor to apply changes
7. change onion addresses references on websites

# update 2023-01-16T20:57:30+01:00 - old onion address removed
the old onion address has been removed.
