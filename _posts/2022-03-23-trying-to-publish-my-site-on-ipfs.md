---
title: trying to publish my site on IPFS
author: jacksonchen666
categories:
- ipfs
- experiment
tags:
- jekyll
date: 2022-03-23 15:44 +0000
last_modified_at: 2022-03-25 16:40:22 +0100
---
so, once upon a time, i tried publishing my website on [IPFS](https://ipfs.io/).
i'm not sure if i wrote a blog post about it.
but the thing is, i followed all the steps, everything worked except, the behavior of the web pages didn't work the same as my GitLab hosted site.

why did it not work the same?

# Jekyll structure output
Jekyll is my SSG for this website.

also, most of the pages on this site is without the `.html` extension.

the structure output really depends if you have a `/` at the end of the path like permalinks, or else you'll get structure like `/posts/2021-12-01/12-36-00.html` instead of `/posts/2021-12-01/12-36-00/index.html`, which is actually harder to deal with due to the extra configuration required to even get it right.
heck, i had to setup nginx for my tor site correctly to handle that mess.

## how web servers handle paths (basically)
here's some example structure from `tree`
```
jekyll-structure-ending-slash
├── index.html
└── posts
    ├── 2022
    │   └── index.html
    └── 2022-03-22
        └── 14-08-10
            └── index.html
jekyll-structure-no-ending-slash
├── index.html
└── posts
    ├── 2022-03-22
    │   └── 14-08-10.html
    └── 2022.html
```

hosting `jekyll-structure-ending-slash` would be as easy as drag-and-dropping into any web server without having to configure it, because most web servers i've encountered (nginx) would check for the `index.html` file (order is usually after trying named file and getting no results).
the result is a website that has extension-less URLs without extra web server configuration.

hosting `jekyll-structure-no-ending-slash` would not work as simple as a drag-and-drop, because it would require extra configuration.
that extra configuration i cannot add to work in IPFS, because it would need to be added everywhere.

## git{hub,lab} pages

thankfully, GitLab pages and GitHub pages both were setup to handle the `/posts/2021-12-01/12-36-00.html` type of output, so most people wouldn't have to deal with any problems.

## the solution to turning a Jekyll site from `jekyll-strucutre-no-ending-slash` to `jekyll-strucutre-ending-slash`
[you must add ending slashes to all permalinks](https://gitlab.com/jacksonchen666/JacksonChen666.gitlab.io/-/compare/dad251dfc636a048345a54ac20a7730eb647364a...d403ecd78b8572aa801e0b98ad223771a50db793), and you can use the following command to find all files that is not named `index.html`: `tree -fi --noreport $DIR -P "*.html" -I "index.html" | grep ".html" --color=never` (substitute `$DIR` for the directory to find files that match the condition)

no plugins required here, just missing the `/` will result in some in `jekyll-structure-no-ending-slash` structure.

# adding the site to IPFS
finally, the part that you've probably been waiting for.

- build the site
- `ipfs add -r _site`
- `ipfs name publish -t 876600h <hash>`
- a control-c
- cannot republish name
- technical difficulties
- checkout to master
- ignore all above
- build site
- `ipfs add -r _site` (final add line contains hash)
- `ipfs name publish -t 876600h <hash>`
- go to the thing

and it's alive!

and it works!!

## the `-t` argument when publishing
you might've (or might'ven't) noticed that i added the `-t` argument when doing `ipfs name publish`. that's because back when i tried, i read a blog post (not sure which anymore) of someone else publishing to ip**ns** (not ip**fs**) and said their thing expired every 12 hours, and had to constantly run the thing to publish it.

so i tried looking in the help of the command and i found that argument on how long the name will work/stay published, and just set it to 100 years.
i don't know if that will cause any problems (hopefully not problems that only waiting for the name to expire is the only solution), but yeah. not much other than "might want to be aware about that!"

# actually publishing the thing
so, i plan to use both [IPNS](https://docs.ipfs.io/concepts/ipns/) to make this all work and just by visiting https://jacksonchen666.com/ should be enough.

however, i'm still doing it manually, so i plan to use gitlab secrets and whatever else i figure out and then finally, automatically publish in gitlab ci.

edit: it has come to my attention that the only way my website will work correctly is if you use a [subdomain gateway](http://docs.ipfs.io.ipns.localhost:8080/how-to/address-ipfs-on-web/#subdomain-gateway), otherwise the website would just not work correctly due to it trying to get things with absolute paths (relative domains).
