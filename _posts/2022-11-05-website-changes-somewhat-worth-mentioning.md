---
layout: post
title: website changes somewhat worth mentioning
author: jacksonchen666
category: [website]
date: 2022-11-05T19:25:40+01:00
last_modified_at: 2022-12-06T20:53:19+01:00
---
hello. we meet again.

is this a conversation? no. this is a blog post.
let me try again.

hi and welcome to this blog post where i talk about website changes and not how i injured myself (i am fine for those who are curious).

this blog post will be about changes i made on my website that are only somewhat noteworthy.
they are noted anyways because they reach the ~~notability threshold~~

# the main header
instead of "Jackson Chen" (which is a pretty generic name tbh), it is now "jacksonchen666".

why? 
you can find me with "jacksonchen666" but not "Jackson Chen".
and someone else might look at the header on this website, search it up, and get confused on where my website is.

# the sub-header
i added a sub-header for random.

# _lowercase_
i lowercased a lot of text on this website. because i am a lowercase person.

it was pretty easy, except avoiding things that should not be lowecased.
doing it in vim, i made a selection in visual mode, then pressed `u`.

note: the `u` command in normal mode is undo.

# be gone `index.html`
you know what's worse than `.html` in a URL? `index.html`.

that was happening on the pagination links (next page, previous page, etc.), so i just substituted `index.html` with "" so the `index.html` thing is no longer present.

i think i have done this already before, but it is somehow no longer there.

# pagination on different types of pages
~~homepage gets 5 posts and the other pages get 20 posts per page.~~
posts shown per page is now 10 instead of being different on other pages (due to an unforeseen bug).

because 5 posts is a little to low for the amount of posts on one page, resulting in 14 pages compared to 4.

this section costed me an additional hour or two after trying to fix [unexpected behaviour from jekyll-paginate-v2](https://github.com/sverrirs/jekyll-paginate-v2/issues/242).
fixing it in my website did not take more time.

# deletions
things that i deleted from this website, because it's useless.

## mentions
i used mentions to link to "author" pages.
there was no other authors on my website, so there wasn't really a point to keep the thing (let alone use it)

## pages listing categories and tags for posts
the page listing the categories and tags for posts is a repeatedly added and removed feature from my website, because i can never really seem to properly categorize and tag my posts, not find a good use for it.

it was never polished. nor used, anywhere.
so it was removed.

if you're interested in checkout the definitely organized tags and categories used across all of my posts,
run `git checkout 3e147cbfb4c5a61241efb875820afc4bcd0d129b^`, `bundle install`, `bundle exec jekyll s` and look at the `/posts/tag` and `/posts/category` directory listings.

# conclusion
```
 91 files changed, 153 insertions(+), 215 deletions(-)
```

those changes are across 69 blog posts (nice) and some other pages and maintenance posts.

the range of the commits is the following:
```
e3dc24c53d1bd9de0a329da375da759e62e228c1..fe70c4407f32413c503f55a3eb88351a19e483c2
```

---
# post writing stats
writing started from 2022-11-05T15:50:49+01:00, it is now 2022-11-05T19:16:45+01:00.
in between that time, i did do other things like eat food, use bathroom, and [try to fix an issue upstream instead](#pagination-on-different-types-of-pages).

proof reading started shortly after, and finished at 2022-11-05T19:25:40+01:00, which became the date for the post.
