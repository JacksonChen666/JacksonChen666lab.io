---
title: New permalink, who dis?
categories: [website]
author: jacksonchen666
date: 2021-08-11 20:19:35 +0200
---
Hi, again. Sorry if you posted a link of one of my post, because now you'll have to change that link to a new format, but should be breaking much less often. How?

# The new format
With the new format, `/posts/$year-$month-$day/$hour-$minute-$second`, how often it will break is likely never ([with exceptions](#caveats-and-exceptions)), because the date is reliant on the publication date, not the modification date (if any).
Changes to the publication date is dependent on the timezone but is also set in stone as the UTC timezone in the configuration file for Jekyll.
So now posts will *really* have a permanent link (unless they were overwritten, like [the post about stupids I've done]({% post_url 2021-07-23-list-of-posts-comments-and-things-where-jacksonchen666-does-stupidity %})).

# Old vs new
Comparing to the old format, `/$category/$year-$month-$day-$title`, changing the category (have done that before) or the title (that too) will break the link.
With those changes that break with the old default format, it cannot break with the new format as the dependent fields are something that are said to be never changed after it's public (publication date).

# Caveats and exceptions
However, while writing this post, I realized that the current permalink is actually flawed for while I write posts because the date actually changes when the post has no date, and so the last modified date is used.
The workaround for that is to set a date.
Previously, I didn't have to.
But now, I do.

