---
migrated: true
title: a dns record i wish i learned more about earlier
date: 2021-12-08 10:32 +0100
author: jacksonchen666
categories: [dns]
tags:
- ddns
---

the cname dns record will save me many api calls to update the ip address of the server. why and how?

well, the cname dns record will need some explaining and some potential use cases defined.

## cname dns record: what does it do?
to put it simply: redirect dns record look up to another domain. doesn't seem to disallow external domains.
this can be useful when the ip address of the server needs to be constantly updated when it changes, and you don't want to cause excessive dns updates (maybe because of rate limiting) or manage many dns records. or you want to use a static site hoster but it would only work by cname dns records.
both are actually my situation.

## limitations
on the 2nd paragraph of section 10.3 of rfc2181 on [this page](https://datatracker.ietf.org/doc/html/rfc2181#section-10.3), Mail eXchange (MX) records and Name Server (NS) records cannot point to cname dns records.

## that's all?
i think so.
