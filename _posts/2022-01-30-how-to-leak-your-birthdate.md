---
layout: post
title: how to leak your birthdate
date: 2022-01-30 23:10:18 +0100
categories: [random]
author: jacksonchen666
---
this is not a guide, but rather the possibilities of leaking your birthdate. sometimes you do it without realizing, even if you didn't blatantly put it out there.
and so, if for whatever reason you don't want to leak your birthdate, i'll cover what could leak your birth date.

# the possible ways to leak your birthdate

1. blatantly obvious - your birthdate as is

that's the first and simplest: just put it out there.

2. celebrate your birthday

yeah... the day and month is now leaked. but what about the year?

you can voluntarily leak your birthdate but without including the year.

2.1. and include your age of you

pretty much, some reverse work can turn out to be the birthdate.

# that's it?
well, i'm not sure if i thought out this well enough to make something long, but who cares.
