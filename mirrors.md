---
title: official webpage mirrors
description: list of links for mirrors of this website
---
```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

mirrors of my main website. currently all by me.
https://jacksonchen666.com
http://x7ikq7gwf6vnbvrc7b36nkcxnw7eckwaricmjbdvrajoeql2ccjb5aad.onion
(use tor browser (no tor2web[^1]), use bookmarks)

no longer maintained/valid:
/ipns/k51qzi5uqu5dh3prgb8bwkdwgsissg4t2fclw6stxgvxtb9qh9bsxerkxpk2kl
https://jacksonchen666.gitlab.io
https://jacksonchen666.github.io

[^1]: https://matt.traudt.xyz/posts/2021-12-02-dont-debug-with-onionto/
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQQFdTkmENxv1Ly2EW3yQfmHGx0cZAUCY9+tDgAKCRDyQfmHGx0c
ZP3YAP40K2qBgKrM0R9dAXDxJUCcPr8KdH3S8m2EASS3tWS/yQEAhM+TR164WxwH
D/AY7tqpJtx4tMlxrbhXI1yAV3eG4wM=
=ZI5G
-----END PGP SIGNATURE-----
```
