<div id="paginator">
<p style="text-align: center;margin-bottom: 0;">page {{ paginator.page }} out of {{ paginator.total_pages }} ({{ paginator.total_posts }} posts)</p>
{% if paginator.previous_page %}
<a href="{{ paginator.previous_page_path | replace:'index.html','' }}" style="float: left;">&larr; newer posts&nbsp;</a>
<a href="{{ paginator.first_page_path | replace:'index.html','' }}" style="float: left;">&larr; &larr; &larr; first page</a>
{% endif %}
{% if paginator.next_page %}
<a href="{{ paginator.next_page_path | replace:'index.html','' }}" style="float: right;">&nbsp;older posts &rarr;</a>
<a href="{{ paginator.last_page_path | replace:'index.html','' }}" style="float: right;">last page &rarr; &rarr; &rarr;</a>
{% endif %}
<br>
<br>
</div>
