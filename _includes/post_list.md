{% for post in include.posts_list offset: include.offset limit: include.amount %}
{% include preview_post.md post=post %}
{% endfor %}
