{% assign post = include.post %}
- ## [{{ post.title }} - {{ post.date | date: "%Y-%m-%d" }}]({{ post.url }}) {% if post.draft %}(Draft){% endif %}
    {% if include.hide_excerpt != true %}{{ post.excerpt | markdownify | strip_html | truncatewords: 15 }} {{ include.add_on_to_excerpt }}{% endif %}
