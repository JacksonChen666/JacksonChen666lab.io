---
title: contact me
description: ways to contact me
---
if you want to share my email address directly, don't. it will change.

[relevant post]({% post_url 2022-01-06-info-about-my-email-address %})

-----
**if you wish to disclose to me anything security related, immediately retrieve my PGP key and contact the main email address for the PGP key while encrypting it.**

-----
preferred ways to contact me:

- email: [alias email](mailto:{% include email.txt %})[^refreshing] (preferred if you want my attention ASAP, passively checked[^passive_check]. you must request for my actual email address if you want to verify my emails with my PGP key (which you should be able to find with my actual email))
    - PGP key id (requires knowledge of actual email address): [786EFFD632E233EF3AB9B9CC9E4FA9055FFD9E30](/786EFFD632E233EF3AB9B9CC9E4FA9055FFD9E30.pgp)

      you may also find other methods of retriving my PGP key, including:
        - [gitlab](https://gitlab.com/JacksonChen666.gpg) (or click the key icon button on the top right below the navigation header when on my account)
        - on [sourcehut](https://meta.sr.ht/~jacksonchen666.pgp)
        - PGP over DNS (use DNSSEC)
        - on the default PGP keyserver for GPG (if you have my *actual* email address)
        - my website, with the link just above (it's on this website)

[^passive_check]: means i don't constantly manually look if there is stuff but i will be notified
[^refreshing]: this can change at anytime. do not assume previous email addresses will work, and use this page in case you don't have my real email address.
